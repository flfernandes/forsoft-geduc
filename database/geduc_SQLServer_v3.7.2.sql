create database geduc
go

USE geduc;

create table pessoa(
	idPessoa INT identity(1,1) PRIMARY KEY,
	nomeP VARCHAR(150),
	dataNascimento DATE,
	sexo varchar(20),
	naturalidade VARCHAR(20),
	nacionalidade varchar(100),
	nomePai varchar(200),
	nomeMae varchar(200),
	etnia VARCHAR(20),
	estadoCivil VARCHAR(20),
	nivelEscolaridade varchar(30),
	necessidadeEsp varchar(20),
	idLogin int,
	idProgramaSocial int,
	idContato int,
	idEndereco int,
	idDocumento int
);

create table telas(
	idTelas int identity(1,1) primary key,
	nome_tela varchar(100),
	app_tela varchar(100),
	permissoes varchar(10)
);

create table sessao(
	idSessao varchar(32) primary key,
	idLogin int,
	perfilAtual varchar(1)
);

create table login(
	idLogin INT identity(1,1) primary key,
	usuario varchar(255) UNIQUE,
	senha varchar(255),
	perfilAcesso varchar(255)
);

create table aluno(
	idAluno INT identity(1,1) PRIMARY KEY,
	matricula varchar(255) NOT NULL UNIQUE,
	situacao VARCHAR(30),
	idCurso int,
	idPessoa int
);
	
create table funcionario(
	idFuncionario INT identity(1,1) PRIMARY KEY,
	matricula varchar(255) NOT NULL UNIQUE,
	situacao VARCHAR(30),
	idCargo int,
	idPessoa int
);

create table documento(
	idDocumento INT identity(1,1) PRIMARY KEY,
	cpf varchar(14) UNIQUE,
	rg varchar(12) UNIQUE,
	dataExpedicao date,
	orgaoExpedidor varchar(100),
	numCertidao varchar(200) UNIQUE,
	livroCertidao varchar(200),
	folhaCertidao varchar(200),
	dataEmiCertidao date,
	titEleitor varchar(100) UNIQUE,
	certReservista varchar(200)
);

create table endereco(
	idEndereco INT identity(1,1) PRIMARY KEY,
	logradouro  VARCHAR (255),
	numero VARCHAR(40),
	complemento VARCHAR (40),
	bairro VARCHAR (255),
	cidade VARCHAR (40),
	uf VARCHAR (40),
	cep VARCHAR (40),
	municipio VARCHAR (40),
	zona VARCHAR (40)
);

create table instituicao(
	idInstituicao INT identity(1,1) primary key,
	nome VARCHAR(255),
	condicao VARCHAR (50),
	codigo VARCHAR (30) UNIQUE,
	localizacao VARCHAR(30),
	dependencia_adm VARCHAR (50),
	suportepcd varchar(20),
	educacaoIndigena VARCHAR (50),
	longitude FLOAT,
	latitude FLOAT,
	data_inicio VARCHAR(30),
	data_fim VARCHAR(30),
	tipo varchar(200),
	idEndereco int,
	idContato int
);

create table setor(
	idSetor INT identity(1,1) primary key,
	descricao varchar(200),
	nomeS varchar(100),
	situacao varchar(30),
	quantidade int,
	idDependencia int,
	idInstituicao int
);

create table turno(
	idTurno INT identity(1,1) primary key,
	turno varchar(200)
);

CREATE TABLE turma(
	idTurma INT identity(1,1) primary key,
	codigoT VARCHAR(100) UNIQUE,
	idTurno int not null
);

CREATE TABLE curso(    
	idCurso INT identity(1,1) primary key,
	nomeC VARCHAR (100),
	descricao VARCHAR (50),
	cargaHoraria INT,
	codigoC VARCHAR (8) UNIQUE,
	modalidadeDeEnsino VARCHAR (50),
	nivelEnsino VARCHAR (50)
);

CREATE TABLE gradecurricular(    
	idGrade INT identity(1,1) primary key,
	cursos TEXT,
	idCurso int not null
);

CREATE TABLE contato(    
	idContato INT identity(1,1) PRIMARY KEY,
	telefoneFixo varchar(20),
	telefoneCelular varchar(20),
	email varchar(200),
	outros text
);

CREATE TABLE disciplina (
	idDisciplina INT identity(1,1) primary key,
	nomeD VARCHAR (40),
	codigoD VARCHAR (20) UNIQUE,
	cargaHoraria INT
);

CREATE TABLE cargo (
	idCargo INT identity(1,1) primary key,
	cargo VARCHAR(50) UNIQUE,
	funcao VARCHAR(50)
);

CREATE TABLE dependencia (
	idDependencia INT identity(1,1) primary key,
	dependencia varchar(200)
);

CREATE TABLE patrimonio(
	idPatrimonio INT identity(1,1) primary key,
	codigoP VARCHAR(30) UNIQUE,
	idTipoPatrimonio INT not null,
	idSetor int
);

CREATE TABLE tipoPatrimonio(
	idtipoPatrimonio INT identity(1,1) primary key,
	tipo varchar(100),
	descricao varchar(200)
);

CREATE TABLE programaSocial(
  idProgramaSocial INT identity(1,1) PRIMARY KEY,
  nomePrograma VARCHAR (200) UNIQUE,
  descricao VARCHAR (250),
  ambitoAdm VARCHAR (10)
);

CREATE TABLE frequencia (
  idFrequencia INT identity(1,1) primary key,
  data DATE,
  situacao VARCHAR (100),
  idAluno INT not null,
  idDisciplina INT
);

create table cargo_funcionario(
	id INT identity(1,1) primary key,
	idCargo int not null,
	idFuncionario int not null
);

create table disciplina_turma(
	id INT identity(1,1) primary key,
	idDisciplina int not null,
	idTurma int not null
);

create table disciplina_professor(
	id INT identity(1,1) primary key,
	idDisciplina int not null,
	idCargo int not null
);

create table grade_curso(
	id INT identity(1,1) primary key,
	idCurso int not null,
	idDisciplina int not null
);

create table cordenador_curso(
	id INT identity(1,1) primary key,
	idCurso int not null,
	idCargo int not null
);

create table turma_curso(
	id INT identity(1,1) primary key,
	idTurma int not null,
	idCurso int not null
);

create table curso_instituicao(
	id INT identity(1,1) primary key,
	idCurso int not null,
	idInstituicao int not null
);

create table turno_curso(
	id INT identity(1,1) primary key,
	idTurno int not null,
	idCurso int not null
);

create table turno_instituicao(
	id INT identity(1,1) primary key,
	idTurno int not null,
	idInstituicao int not null
);

create table aluno_responsavel(
	id INT identity(1,1) primary key,
	idAluno int not null,
	idPessoa int not null,
	grauParentesco varchar(100),
	responsavel varchar(10)
);

create table setor_funcionario(
	id INT identity(1,1) primary key,
	idFuncionario int not null,
	idSetor int not null
);

create table nota_aluno(
	id INT identity(1,1) PRIMARY KEY,
	idAluno int,
	idDisciplina int,
	nota float,
	periodo varchar(50),
	origem varchar(200),
	data date
);

create table aluno_turma(
	id INT identity(1,1) PRIMARY KEY,
	idAluno int,
	idTurma int
);


alter table pessoa
add constraint fk_pessoa_login
foreign key (idLogin) references login(idLogin),
constraint fk_pessoa_programa
foreign key (idProgramaSocial) references programaSocial(idProgramaSocial),
constraint fk_pessoa_contato
foreign key (idContato) references contato(idContato),
constraint fk_pessoa_endereco
foreign key (idEndereco) references endereco(idEndereco),
constraint fk_pessoa_documento
foreign key (idDocumento) references documento(idDocumento);

alter table aluno
add constraint fk_aluno_curso
foreign key (idCurso) references curso(idCurso),
constraint fk_aluno_pessoa
foreign key (idPessoa) references pessoa(idPessoa);

alter table funcionario
add constraint fk_funcionario_cargo
foreign key (idCargo) references cargo(idCargo),
constraint fk_funcionario_pessoa
foreign key (idPessoa) references pessoa(idPessoa);

alter table gradecurricular
add constraint fk_grade_curso
foreign key (idCurso) references curso(idCurso) ON DELETE CASCADE;

alter table instituicao
add constraint fk_instituicao_endereco
foreign key (idEndereco) references endereco(idEndereco),
constraint fk_instituicao_contato
foreign key (idContato) references contato(idContato);

alter table setor
add constraint fk_setor_dependencia
foreign key (idDependencia) references dependencia(idDependencia),
constraint fk_setor_instituicao
foreign key (idInstituicao) references instituicao(idInstituicao) ON DELETE CASCADE;

alter table turno_instituicao
add constraint fk_ti_instituicao
foreign key (idInstituicao) references instituicao(idInstituicao),
constraint fk_ti_turno
foreign key (idTurno) references turno(idTurno);

alter table turma
add constraint fk_turma_turno
foreign key (idTurno) references turno(idTurno);

alter table patrimonio
add constraint fk_patrimonio_setor
foreign key (idSetor) references setor(idSetor),
constraint fk_patrimonio_tipo
foreign key (idTipoPatrimonio) references tipoPatrimonio(idTipoPatrimonio) ON DELETE CASCADE;

alter table frequencia
add constraint fk_frequencia_aluno
foreign key (idAluno) references aluno(idAluno),
constraint fk_frequencia_disciplina
foreign key (idDisciplina) references disciplina(idDisciplina);

alter table cargo_funcionario
add constraint fk_cf_cargo
foreign key (idCargo) references cargo(idCargo),
constraint fk_cf_funcionario
foreign key (idFuncionario) references funcionario(idFuncionario);

alter table disciplina_turma
add constraint fk_dt_disciplina
foreign key (idDisciplina) references disciplina(idDisciplina),
constraint fk_dt_turma
foreign key (idTurma) references turma(idTurma);

alter table disciplina_professor
add constraint fk_dprof_disciplina
foreign key (idDisciplina) references disciplina(idDisciplina),
constraint fk_dprof_cargo
foreign key (idCargo) references cargo(idCargo);

alter table grade_curso
add constraint fk_gc_disciplina
foreign key (idDisciplina) references disciplina(idDisciplina),
constraint fk_gc_curso
foreign key (idCurso) references curso(idCurso);

alter table cordenador_curso
add constraint fk_cc_cordenador
foreign key (idCargo) references cargo(idCargo),
constraint fk_cc_curso
foreign key (idCurso) references curso(idCurso);

alter table turma_curso
add constraint fk_tc_turma
foreign key (idTurma) references turma(idTurma),
constraint fk_tc_curso
foreign key (idCurso) references curso(idCurso);

alter table curso_instituicao
add constraint fk_cursoinsti_instituicao
foreign key (idInstituicao) references instituicao(idInstituicao),
constraint fk_cursoinsti_curso
foreign key (idCurso) references curso(idCurso);

alter table turno_curso
add constraint fk_turnoc_turno
foreign key (idTurno) references turno(idTurno),
constraint fk_turnoc_curso
foreign key (idCurso) references curso(idCurso);

alter table aluno_responsavel
add constraint fk_ar_aluno
foreign key (idAluno) references aluno(idAluno),
constraint fk_ar_responsavel
foreign key (idPessoa) references pessoa(idPessoa);

alter table setor_funcionario
add constraint fk_sf_setor
foreign key (idSetor) references setor(idSetor),
constraint fk_sf_funcionario
foreign key (idFuncionario) references funcionario(idFuncionario);

alter table nota_aluno
add constraint fk_na_aluno
foreign key (idAluno) references aluno(idAluno),
constraint fk_na_nota
foreign key (idDisciplina) references disciplina(idDisciplina);

alter table aluno_turma
add constraint fk_at_aluno
foreign key (idAluno) references aluno(idAluno),
constraint fk_at_turma
foreign key (idTurma) references turma(idTurma);

insert into programaSocial (nomePrograma) values ('Nenhum');

insert into telas(nome_tela, app_tela, permissoes)
values ('Alterar Disciplina', 'alterar_disciplina.aspx', '-----FGH-J');

insert into telas(nome_tela, app_tela, permissoes)
values ('Alterar Instituicao', 'alterar_instituicao.jsp', '---DEFGH-J');

insert into telas(nome_tela, app_tela, permissoes)
values ('Alterar Turma', 'alterar_turma.jsp', '--CDE----J');

insert into telas(nome_tela, app_tela, permissoes)
values ('Buscar Instituicao', 'buscar_instituicao.jsp', 'ABCDEFGHIJ');

insert into telas(nome_tela, app_tela, permissoes)
values ('Buscar Turma', 'buscar_turma.jsp', 'ABCDE----J');

insert into telas(nome_tela, app_tela, permissoes)
values ('Cadastrar Curso', 'cadastrar_curso.jsp', '--CDEFGH-J');

insert into telas(nome_tela, app_tela, permissoes)
values ('Cadastrar Instituicao', 'cadastrar_instituicao.jsp', '-----FGH-J');

insert into telas(nome_tela, app_tela, permissoes)
values ('Cadastrar Setor', 'cadastrar_setor.jsp', '--CDE----J');

insert into telas(nome_tela, app_tela, permissoes)
values ('Cadastrar Turma', 'cadastrar_turma.jsp', '--CDE----J');

insert into telas(nome_tela, app_tela, permissoes)
values ('Listar Curso', 'listar_curso.jsp', 'ABCDEFGHIJ');

insert into telas(nome_tela, app_tela, permissoes)
values ('Listar Instituicao', 'listar_instituicao.jsp', 'ABCDEFGHIJ');

insert into telas(nome_tela, app_tela, permissoes)
values ('Listar Turmas', 'listar_turma.jsp', 'ABCDE----J');

insert into telas(nome_tela, app_tela, permissoes)
values ('Home', 'home.jsp', 'ABCDEFGHIJ');

insert into telas(nome_tela, app_tela, permissoes)
values ('Index', 'index.jsp', 'ABCDEFGHIJ');

insert into telas(nome_tela, app_tela, permissoes)
values ('Login', 'login.jsp', 'ABCDEFGHIJ');

insert into telas(nome_tela, app_tela, permissoes)
values ('Alterar Disciplina', 'alterar_disciplina.aspx', '-----FGH-J');

insert into telas(nome_tela, app_tela, permissoes)
values ('Alterar Aluno', 'alterar_aluno.aspx', '--CDE----J');

insert into telas(nome_tela, app_tela, permissoes)
values ('Alterar Funcionario', 'alterar_funcionario.aspx', '-----FGH-J');

insert into telas(nome_tela, app_tela, permissoes)
values ('Alterar Nota', 'altera_nota.aspx', '-B-------J');

insert into telas(nome_tela, app_tela, permissoes)
values ('Cadastrar Aluno', 'cadastro_aluno.aspx', '--CDE----J');

insert into telas(nome_tela, app_tela, permissoes)
values ('Cadastrar Disciplinas', 'cadastro_disciplina.aspx', '-----FGH-J');

insert into telas(nome_tela, app_tela, permissoes)
values ('Cadastrar Funcionario', 'cadastro_funcionario.aspx', '-----FGH-J');

insert into telas(nome_tela, app_tela, permissoes)
values ('Cadastrar Nota', 'cadastro_nota.aspx', '---D-----J');

insert into telas(nome_tela, app_tela, permissoes)
values ('Cadastrar Responsavel', 'cadastro_responsavel.aspx', '--CDE----J');

insert into telas(nome_tela, app_tela, permissoes)
values ('Consultar Nota', 'consultar_nota.aspx', 'ABCDE----J');

insert into telas(nome_tela, app_tela, permissoes)
values ('Consultar Disciplina', 'consultar_disciplina.aspx', 'ABCDE----J');

insert into telas(nome_tela, app_tela, permissoes)
values ('Consultar Funcionario', 'consultar_funcionario.aspx', '-BCDE----J');

insert into telas(nome_tela, app_tela, permissoes)
values ('Listar Alunos', 'listar_aluno.aspx', 'ABCDE----J');

insert into telas(nome_tela, app_tela, permissoes)
values ('Listar Disciplinas', 'listar_disciplina.aspx', 'ABCDE----J');

insert into telas(nome_tela, app_tela, permissoes)
values ('Listar Funcionarios', 'listar_funcionario.aspx', '-BCDE----J');

insert into telas(nome_tela, app_tela, permissoes)
values ('Listar Notas', 'listar_notas.aspx', '-BCDE----J');

insert into telas(nome_tela, app_tela, permissoes)
values ('Cadastro Cargo', 'cadastro_cargo.aspx', '-----FGH-J');

insert into telas(nome_tela, app_tela, permissoes)
values ('Cadastro Projeto Social', 'cadastro_projetosocial.aspx', '--CDEFGH-J');

insert into telas (nome_tela, app_tela, permissoes)
values ('Alterar Curso', 'alterar_curso.jsp','----E-GH-J'),
('Alterar Frequencia Aluno', 'alterar_frequencia_aluno.jsp','-BC------J'),
('Alterar Frequencia Turma', 'alterar_frequencia_turma.jsp', '-BC------J'),
('Cadastrar Frequencia', 'cadastrar_frequencia.jsp','-B-------J'),
('Cadastrar Frequencia Lista de Alunos','cadastrar_frequencia_lista_alunos.jsp', '-B-------J'),
('Configuraçoes', 'config.jsp', '--CDEFGHIJ'),
('Listar frequencia retorno turma','listar_frequencia_retorno.jsp','ABCD-FG-J'),
('Listar frequencia', 'listar_frequencia.jsp', 'ABCD-FG-J'),
('Listar frequencia retorno aluno','listar_frequencia_retorno_aluno', 'ABCD-FG-J'),
('Listar setores', 'listar_setors,jsp', '--C-EFGH-J'),
('Negar acesso','nao_pode_acessar.jsp', 'ABCDEFGHIJ'),
('Resultado buscar instituicao','resultado_buscar_instituicao.jsp', 'ABCDEFGH-J'),
('Resultado buscar turma','resultado_buscar_turma.jsp', 'ABCD-----J'),
('Resultado listar curso', 'resultado_listar_curso.jsp', '--CDEFGHIJ'),
('Resultado listar instituicao','resultado_listar_instituicao.jsp', 'ABCDEFGH-J'),
('Resultado listar turma','resultado_listar_turma.jsp', 'ABCD-----J');