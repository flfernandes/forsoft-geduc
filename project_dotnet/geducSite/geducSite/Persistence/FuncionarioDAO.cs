﻿using geducSite.Context;
using geducSite.Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace geducSite.Persistence
{
    public class FuncionarioDAO : Conexao
    { //Método para gravar o usuario na base de dados...
        public void Salvar(Funcionario f)
        {
            try
            {
                AbrirConexao();

                myCmd = new MySqlCommand("INSERT INTO contato (telefoneFixo, telefoneCelular,email, outros) values(@v1, @v2, @v3, @v4); SELECT LAST_INSERT_ID();", myCon);
                myCmd.Parameters.AddWithValue("@v1", f.contato.telefoneFixo);
                myCmd.Parameters.AddWithValue("@v2", f.contato.telefoneCelular);
                myCmd.Parameters.AddWithValue("@v3", f.contato.email);
                myCmd.Parameters.AddWithValue("@v4", f.contato.outros);
                f.contato.idContato = Convert.ToInt32(myCmd.ExecuteScalar());

                //Gravar Contato
                cmd = new SqlCommand("insert into contato(telefoneFixo, telefoneCelular,email, outros) values(@v1, @v2, @v3, @v4) SELECT SCOPE_IDENTITY()", con);
                cmd.Parameters.AddWithValue("@v1", f.contato.telefoneFixo);
                cmd.Parameters.AddWithValue("@v2", f.contato.telefoneCelular);
                cmd.Parameters.AddWithValue("@v3", f.contato.email);
                cmd.Parameters.AddWithValue("@v4", f.contato.outros);
                f.contato.idContato = Convert.ToInt32(cmd.ExecuteScalar());

                //Documento
                myCmd = new MySqlCommand("insert into documento(cpf,rg,dataExpedicao,orgaoExpedidor,numCertidao,livroCertidao,folhaCertidao,dataEmiCertidao,titEleitor,certReservista) values(@v1, @v2, @v3, @v4, @v5, @v6, @v7, @v8, @v9, @v10); SELECT LAST_INSERT_ID();", myCon);
                myCmd.Parameters.AddWithValue("@v1", f.documento.certReservista);
                myCmd.Parameters.AddWithValue("@v2", f.documento.rg);
                myCmd.Parameters.AddWithValue("@v3", f.documento.dataExpedicao);
                myCmd.Parameters.AddWithValue("@v4", f.documento.orgaoExpedidor);
                myCmd.Parameters.AddWithValue("@v5", f.documento.numCertidao);
                myCmd.Parameters.AddWithValue("@v6", f.documento.livroCertidao);
                myCmd.Parameters.AddWithValue("@v7", f.documento.folhaCertidao);
                myCmd.Parameters.AddWithValue("@v8", f.documento.dataEmiCertidao);
                myCmd.Parameters.AddWithValue("@v9", f.documento.titEleitor);
                myCmd.Parameters.AddWithValue("@v10", f.documento.certReservista);
                f.documento.idDocumento = Convert.ToInt32(myCmd.ExecuteScalar());
                //Documento
                cmd = new SqlCommand("insert into documento(cpf,rg,dataExpedicao,orgaoExpedidor,numCertidao,livroCertidao,folhaCertidao,dataEmiCertidao,titEleitor,certReservista) values(@v1, @v2, @v3, @v4, @v5, @v6, @v7, @v8, @v9, @v10) SELECT SCOPE_IDENTITY()", con);
                cmd.Parameters.AddWithValue("@v1", f.documento.certReservista);
                cmd.Parameters.AddWithValue("@v2", f.documento.rg);
                cmd.Parameters.AddWithValue("@v3", f.documento.dataExpedicao);
                cmd.Parameters.AddWithValue("@v4", f.documento.orgaoExpedidor);
                cmd.Parameters.AddWithValue("@v5", f.documento.numCertidao);
                cmd.Parameters.AddWithValue("@v6", f.documento.livroCertidao);
                cmd.Parameters.AddWithValue("@v7", f.documento.folhaCertidao);
                cmd.Parameters.AddWithValue("@v8", f.documento.dataEmiCertidao);
                cmd.Parameters.AddWithValue("@v9", f.documento.titEleitor);
                cmd.Parameters.AddWithValue("@v10", f.documento.certReservista);
                f.documento.idDocumento = Convert.ToInt32(cmd.ExecuteScalar());

                //gravar o endereço
                myCmd = new MySqlCommand("insert into endereco(logradouro, numero, complemento, bairro, cidade, uf, cep,municipio, zona) values(@v1, @v2, @v3, @v4, @v5,@v6,@v7,@v8,@v9); SELECT LAST_INSERT_ID();", myCon);
                myCmd.Parameters.AddWithValue("@v1", f.endereco.longradouro);
                myCmd.Parameters.AddWithValue("@v2", f.endereco.numero);
                myCmd.Parameters.AddWithValue("@v3", f.endereco.complemento);
                myCmd.Parameters.AddWithValue("@v4", f.endereco.bairro);
                myCmd.Parameters.AddWithValue("@v5", f.endereco.cidade);
                myCmd.Parameters.AddWithValue("@v6", f.endereco.uf);
                myCmd.Parameters.AddWithValue("@v7", f.endereco.cep);
                myCmd.Parameters.AddWithValue("@v8", f.endereco.municipio);
                myCmd.Parameters.AddWithValue("@v9", f.endereco.zona);
                f.endereco.idEndereco = Convert.ToInt32(myCmd.ExecuteScalar());
                //gravar o endereço
                cmd = new SqlCommand("insert into endereco(logradouro, numero, complemento, bairro, cidade, uf, cep,municipio, zona) values(@v1, @v2, @v3, @v4, @v5,@v6,@v7,@v8,@v9) SELECT SCOPE_IDENTITY()", con);
                cmd.Parameters.AddWithValue("@v1", f.endereco.longradouro);
                cmd.Parameters.AddWithValue("@v2", f.endereco.numero);
                cmd.Parameters.AddWithValue("@v3", f.endereco.complemento);
                cmd.Parameters.AddWithValue("@v4", f.endereco.bairro);
                cmd.Parameters.AddWithValue("@v5", f.endereco.cidade);
                cmd.Parameters.AddWithValue("@v6", f.endereco.uf);
                cmd.Parameters.AddWithValue("@v7", f.endereco.cep);
                cmd.Parameters.AddWithValue("@v8", f.endereco.municipio);
                cmd.Parameters.AddWithValue("@v9", f.endereco.zona);
                f.endereco.idEndereco = Convert.ToInt32(cmd.ExecuteScalar());

                //apenas para testes e a apresentação de sexta
                myCmd = new MySqlCommand("INSERT INTO login (`usuario`, `senha`, `perfilAcesso`) VALUES (@v1, @v2, @v3); SELECT LAST_INSERT_ID();", myCon);
                myCmd.Parameters.AddWithValue("@v1", f.login.usuario);
                myCmd.Parameters.AddWithValue("@v2", f.login.senha);
                myCmd.Parameters.AddWithValue("@v3", f.login.perfilAcesso);
                f.login.idLogin = Convert.ToInt32(myCmd.ExecuteScalar());
                //apenas para testes e a apresentação de sexta
                cmd = new SqlCommand("insert into login values(@v1, @v2, @v3)SELECT SCOPE_IDENTITY()", con);
                cmd.Parameters.AddWithValue("@v1", f.login.usuario);
                cmd.Parameters.AddWithValue("@v2", f.login.senha);
                cmd.Parameters.AddWithValue("@v3", f.login.perfilAcesso);
                f.login.idLogin = Convert.ToInt32(cmd.ExecuteScalar());
                
                //apenas para testes e a apresentação de sexta
                cmd = new SqlCommand("insert into login values(@v1, @v2, @v3)SELECT SCOPE_IDENTITY()", con);
                cmd.Parameters.AddWithValue("@v1", f.login.usuario);
                cmd.Parameters.AddWithValue("@v2", f.login.senha);
                cmd.Parameters.AddWithValue("@v3", f.login.perfilAcesso);
                f.login.idLogin = Convert.ToInt32(cmd.ExecuteScalar());

                //Pessoa
                myCmd = new MySqlCommand("INSERT INTO  pessoa (`nomeP`, `dataNascimento`, `sexo`, `naturalidade`, `nacionalidade`, `nomePai`, `nomeMae`, `etnia`, `estadoCivil`, `nivelEscolaridade`, `necessidadeEsp`, `idLogin`, `idProgramaSocial`, `idContato`, `idEndereco`, `idDocumento`) VALUES (@v1, @v2, @v3, @v4, @v5, @v6, @v7, @v8, @v9, @v10, @v11, @v12, @v13, @v14, @v15, @v16); SELECT LAST_INSERT_ID();", myCon);
                myCmd.Parameters.AddWithValue("@v1", f.nome);
                myCmd.Parameters.AddWithValue("@v2", f.dataNascimento);
                myCmd.Parameters.AddWithValue("@v3", f.sexo);
                myCmd.Parameters.AddWithValue("@v4", f.naturalidade);
                myCmd.Parameters.AddWithValue("@v5", f.nacionalidade);
                myCmd.Parameters.AddWithValue("@v6", f.nomePai);
                myCmd.Parameters.AddWithValue("@v7", f.nomeMae);
                myCmd.Parameters.AddWithValue("@v8", f.etnia);
                myCmd.Parameters.AddWithValue("@v9", f.estadoCivil);
                myCmd.Parameters.AddWithValue("@v10", f.nivelEscolaridade);
                myCmd.Parameters.AddWithValue("@v11", f.necessidadeEsp);
                myCmd.Parameters.AddWithValue("@v12", f.login.idLogin);
                if (f.programaSocial.idProgramaSocial <= 0)
                {
                    myCmd.Parameters.AddWithValue("@v13", 1);
                }
                else if (f.programaSocial.idProgramaSocial >= 1)
                {
                    myCmd.Parameters.AddWithValue("@v13", f.programaSocial.idProgramaSocial);
                }
                myCmd.Parameters.AddWithValue("@v14", f.contato.idContato);
                myCmd.Parameters.AddWithValue("@v15", f.endereco.idEndereco);
                myCmd.Parameters.AddWithValue("@v16", f.documento.idDocumento);
                f.idPessoa = Convert.ToInt32(myCmd.ExecuteScalar());
                //Pessoa
                cmd = new SqlCommand("INSERT INTO pessoa VALUES(@v1, @v2, @v3, @v4, @v5, @v6, @v7, @v8, @v9, @v10, @v11, @v12, @v13, @v14, @v15, @v16) SELECT SCOPE_IDENTITY()", con);
                cmd.Parameters.AddWithValue("@v1", f.nome);
                cmd.Parameters.AddWithValue("@v2", f.dataNascimento);
                cmd.Parameters.AddWithValue("@v3", f.sexo);
                cmd.Parameters.AddWithValue("@v4", f.naturalidade);
                cmd.Parameters.AddWithValue("@v5", f.nacionalidade);
                cmd.Parameters.AddWithValue("@v6", f.nomePai);
                cmd.Parameters.AddWithValue("@v7", f.nomeMae);
                cmd.Parameters.AddWithValue("@v8", f.etnia);
                cmd.Parameters.AddWithValue("@v9", f.estadoCivil);
                cmd.Parameters.AddWithValue("@v10", f.nivelEscolaridade);
                cmd.Parameters.AddWithValue("@v11", f.necessidadeEsp);
                cmd.Parameters.AddWithValue("@v12", f.login.idLogin);
                if (f.programaSocial.idProgramaSocial <= 0)
                {
                    cmd.Parameters.AddWithValue("@v13", 1);
                }
                else if (f.programaSocial.idProgramaSocial >= 1)
                {
                    cmd.Parameters.AddWithValue("@v13", f.programaSocial.idProgramaSocial);
                }
                cmd.Parameters.AddWithValue("@v14", f.contato.idContato);
                cmd.Parameters.AddWithValue("@v15", f.endereco.idEndereco);
                cmd.Parameters.AddWithValue("@v16", f.documento.idDocumento);
                f.idPessoa = Convert.ToInt32(cmd.ExecuteScalar());

                //gravar o funcionario junto com pessoa, código precisa ser melhorado, mas para ter um noção tá valendo
                myCmd = new MySqlCommand("INSERT INTO funcionario (`matricula`, `situacao`, `idCargo`, `idPessoa`) VALUES (@v1,@v2,@v3,@v4); SELECT LAST_INSERT_ID();", myCon);
                myCmd.Parameters.AddWithValue("@v1", f.matricula);
                myCmd.Parameters.AddWithValue("@v2", f.situacao);
                myCmd.Parameters.AddWithValue("@v3", f.cargo.idCargo);
                myCmd.Parameters.AddWithValue("@v4", f.idPessoa);
                f.idFuncionario = Convert.ToInt32(myCmd.ExecuteScalar());
                //gravar o funcionario junto com pessoa, código precisa ser melhorado, mas para ter um noção tá valendo
                cmd = new SqlCommand("insert into funcionario values(@v1,@v2,@v3,@v4) SELECT SCOPE_IDENTITY()", con);
                cmd.Parameters.AddWithValue("@v1", f.matricula);
                cmd.Parameters.AddWithValue("@v2", f.situacao);
                cmd.Parameters.AddWithValue("@v3", f.cargo.idCargo);
                cmd.Parameters.AddWithValue("@v4", f.idPessoa);
                f.idFuncionario = Convert.ToInt32(cmd.ExecuteScalar());

                myCmd = new MySqlCommand("INSERT INTO `geduc`.`cargo_funcionario` (`idCargo`, `idFuncionario`) VALUES (@v1,@v2)", myCon);
                myCmd.Parameters.AddWithValue("@v1", f.cargo.idCargo);
                myCmd.Parameters.AddWithValue("@v2", f.idFuncionario);
                myCmd.ExecuteNonQuery();
                 cmd = new SqlCommand("insert into cargo_funcionario values(@v1,@v2)", con);
                cmd.Parameters.AddWithValue("@v1", f.cargo.idCargo);
                cmd.Parameters.AddWithValue("@v2", f.idFuncionario);
                cmd.ExecuteNonQuery();

            }
            catch
            {
                throw;
            }
            finally
            {
                FecharConexao();
            }
        }

        public IEnumerable<Funcionario> Listar()
        {
            try
            {
                AbrirConexao();

                cmd = new SqlCommand("SELECT f.idFuncionario,ca.idCargo,p.idPessoa,co.idContato,e.idEndereco,l.idLogin ,ps.idProgramaSocial,ps.descricao,ps.nomePrograma,ps.ambitoAdm,f.matricula,d.idDocumento,l.usuario,l.senha,l.perfilAcesso ,p.nomeP,p.dataNascimento,p.sexo,p.naturalidade,p.nacionalidade,p.nomePai,p.nomeMae,p.etnia,p.estadoCivil,p.nivelEscolaridade,p.necessidadeEsp,ca.cargo,ca.funcao,f.situacao,co.telefoneFixo,co.telefoneCelular,co.email,co.outros,e.logradouro,e.numero,e.complemento,e.bairro,e.cidade,e.uf,e.cep,e.municipio,e.zona,d.cpf,d.rg,d.dataExpedicao,d.orgaoExpedidor,d.numCertidao,d.livroCertidao,d.folhaCertidao,d.dataEmiCertidao,d.titEleitor,d.certReservista FROM funcionario f INNER JOIN pessoa p ON f.idPessoa = p.idPessoa INNER JOIN cargo ca ON f.idCargo = ca.idCargo INNER JOIN contato co ON p.idContato = co.idContato  INNER JOIN endereco e ON p.idEndereco = e.idEndereco  INNER JOIN documento d ON p.idDocumento = d.idDocumento INNER JOIN login l ON l.idLogin = p.idLogin INNER JOIN programaSocial ps ON ps.idProgramaSocial = ps.idProgramaSocial", con);
                dr = cmd.ExecuteReader();

                Collection<Funcionario> funcionario = new Collection<Funcionario>();
                while (dr.Read())
                {
                    Funcionario f = new Funcionario();
                    Cargo ca = new Cargo();
                    Endereco e = new Endereco();
                    Documento d = new Documento();
                    Contato co = new Contato();
                    Login l = new Login();
                    ProgramaSocial ps = new ProgramaSocial();

                    f.idFuncionario = Convert.ToInt32(dr["idFuncionario"]);
                    f.matricula = Convert.ToString(dr["idFuncionario"]);
                    f.nome = Convert.ToString(dr["nomeP"]);
                    f.dataNascimento = Convert.ToDateTime(dr["dataNascimento"]);
                    f.sexo = Convert.ToString(dr["sexo"]);
                    f.naturalidade = Convert.ToString(dr["naturalidade"]);
                    f.nacionalidade = Convert.ToString(dr["nacionalidade"]);
                    f.nomePai = Convert.ToString(dr["nomePai"]);
                    f.nomeMae = Convert.ToString(dr["nomeMae"]);
                    f.etnia = Convert.ToString(dr["etnia"]);
                    f.estadoCivil = Convert.ToString(dr["estadoCivil"]);
                    f.nivelEscolaridade = Convert.ToString(dr["nivelEscolaridade"]);
                    f.necessidadeEsp = Convert.ToString(dr["necessidadeEsp"]);
                    f.situacao = Convert.ToString(dr["situacao"]);
                    f.idPessoa = Convert.ToInt32(dr["idPessoa"]);
                    ca.idCargo = Convert.ToInt32(dr["idCargo"]);
                    co.idContato = Convert.ToInt32(dr["idContato"]);
                    e.idEndereco = Convert.ToInt32(dr["idEndereco"]);
                    d.idDocumento = Convert.ToInt32(dr["idDocumento"]);
                    l.idLogin = Convert.ToInt32(dr["idLogin"]);
                    l.usuario = Convert.ToString(dr["usuario"]);
                    l.senha = Convert.ToString(dr["senha"]);
                    l.perfilAcesso = Convert.ToString(dr["perfilAcesso"]);
                    ps.idProgramaSocial = Convert.ToInt32(dr["idProgramaSocial"]);
                    ps.descricao = Convert.ToString(dr["descricao"]);
                    ps.nomePrograma = Convert.ToString(dr["nomePrograma"]);
                    ps.ambitoAdm = Convert.ToString(dr["ambitoAdm"]);
                    ca.cargo = Convert.ToString(dr["cargo"]);
                    ca.funcao = Convert.ToString(dr["funcao"]);
                    co.telefoneFixo = Convert.ToString(dr["telefoneFixo"]);
                    co.telefoneCelular = Convert.ToString(dr["telefoneCelular"]);
                    co.email = Convert.ToString(dr["email"]);
                    co.outros = Convert.ToString(dr["outros"]);
                    e.longradouro = Convert.ToString(dr["logradouro"]);
                    e.numero = Convert.ToString(dr["numero"]);
                    e.complemento = Convert.ToString(dr["complemento"]);
                    e.bairro = Convert.ToString(dr["bairro"]);
                    e.cidade = Convert.ToString(dr["cidade"]);
                    e.uf = Convert.ToString(dr["uf"]);
                    e.cep = Convert.ToString(dr["cep"]);
                    e.municipio = Convert.ToString(dr["municipio"]);
                    e.zona = Convert.ToString(dr["zona"]);
                    d.cpf = Convert.ToString(dr["cpf"]);
                    d.rg = Convert.ToString(dr["rg"]);
                    d.dataExpedicao = Convert.ToDateTime(dr["dataExpedicao"]);
                    d.orgaoExpedidor = Convert.ToString(dr["orgaoExpedidor"]);
                    d.numCertidao = Convert.ToString(dr["numCertidao"]);
                    d.livroCertidao = Convert.ToString(dr["numCertidao"]);
                    d.folhaCertidao = Convert.ToString(dr["folhaCertidao"]);
                    d.dataEmiCertidao = Convert.ToDateTime(dr["dataEmiCertidao"]);
                    d.titEleitor = Convert.ToString(dr["titEleitor"]);
                    d.certReservista = Convert.ToString(dr["certReservista"]);

                    f.contato = co;
                    f.cargo = ca;
                    f.endereco = e;
                    f.documento = d;
                    f.login = l;
                    f.programaSocial = ps;

                    funcionario.Add(f);
                }
                return funcionario;
            }
            finally
            {
                FecharConexao();
            }
        }

        public void Update(Funcionario f)
        {
            try
            {
                AbrirConexao();


                //Gravar Contato
                myCmd = new MySqlCommand("UPDATE contato SET telefoneFixo = @v1, telefoneCelular = @v2,email = @v3, outros = @v4 WHERE idContato = @v5", myCon);
                myCmd.Parameters.AddWithValue("@v1", f.contato.telefoneFixo);
                myCmd.Parameters.AddWithValue("@v2", f.contato.telefoneCelular);
                myCmd.Parameters.AddWithValue("@v3", f.contato.email);
                myCmd.Parameters.AddWithValue("@v4", f.contato.outros);
                myCmd.Parameters.AddWithValue("@v5", f.contato.idContato);
                myCmd.ExecuteNonQuery();
                //Gravar Contato
                cmd = new SqlCommand("UPDATE contato SET telefoneFixo = @v1, telefoneCelular = @v2,email = @v3, outros = @v4 WHERE idContato = @v5", con);
                cmd.Parameters.AddWithValue("@v1", f.contato.telefoneFixo);
                cmd.Parameters.AddWithValue("@v2", f.contato.telefoneCelular);
                cmd.Parameters.AddWithValue("@v3", f.contato.email);
                cmd.Parameters.AddWithValue("@v4", f.contato.outros);
                cmd.Parameters.AddWithValue("@v5", f.contato.idContato);
                cmd.ExecuteNonQuery();

                //Documento
                myCmd = new MySqlCommand("UPDATE documento SET cpf=@v1,rg=@v2,dataExpedicao=@v3,orgaoExpedidor=@v4,numCertidao=@v5,livroCertidao=@v6,folhaCertidao=@v7,dataEmiCertidao=@v8,titEleitor=@v9,certReservista=@v10 WHERE idDocumento = @v11", myCon);
                myCmd.Parameters.AddWithValue("@v1", f.documento.cpf);
                myCmd.Parameters.AddWithValue("@v2", f.documento.rg);
                myCmd.Parameters.AddWithValue("@v3", f.documento.dataExpedicao);
                myCmd.Parameters.AddWithValue("@v4", f.documento.orgaoExpedidor);
                myCmd.Parameters.AddWithValue("@v5", f.documento.numCertidao);
                myCmd.Parameters.AddWithValue("@v6", f.documento.livroCertidao);
                myCmd.Parameters.AddWithValue("@v7", f.documento.folhaCertidao);
                myCmd.Parameters.AddWithValue("@v8", f.documento.dataEmiCertidao);
                myCmd.Parameters.AddWithValue("@v9", f.documento.titEleitor);
                myCmd.Parameters.AddWithValue("@v10", f.documento.certReservista);
                myCmd.Parameters.AddWithValue("@v11", f.documento.idDocumento);
                myCmd.ExecuteNonQuery();   
                //Documento
                cmd = new SqlCommand("UPDATE documento SET cpf=@v1,rg=@v2,dataExpedicao=@v3,orgaoExpedidor=@v4,numCertidao=@v5,livroCertidao=@v6,folhaCertidao=@v7,dataEmiCertidao=@v8,titEleitor=@v9,certReservista=@v10 WHERE idDocumento = @v11", con);
                cmd.Parameters.AddWithValue("@v1", f.documento.cpf);
                cmd.Parameters.AddWithValue("@v2", f.documento.rg);
                cmd.Parameters.AddWithValue("@v3", f.documento.dataExpedicao);
                cmd.Parameters.AddWithValue("@v4", f.documento.orgaoExpedidor);
                cmd.Parameters.AddWithValue("@v5", f.documento.numCertidao);
                cmd.Parameters.AddWithValue("@v6", f.documento.livroCertidao);
                cmd.Parameters.AddWithValue("@v7", f.documento.folhaCertidao);
                cmd.Parameters.AddWithValue("@v8", f.documento.dataEmiCertidao);
                cmd.Parameters.AddWithValue("@v9", f.documento.titEleitor);
                cmd.Parameters.AddWithValue("@v10", f.documento.certReservista);
                cmd.Parameters.AddWithValue("@v11", f.documento.idDocumento);
                cmd.ExecuteNonQuery();

                //login
                myCmd = new MySqlCommand("UPDATE login SET usuario = @v1,senha = @v2,perfilAcesso = @v3 WHERE idLogin = @v4 ", myCon);
                myCmd.Parameters.AddWithValue("@v1", f.login.usuario);
                myCmd.Parameters.AddWithValue("@v2", f.login.senha);
                myCmd.Parameters.AddWithValue("@v3", f.login.perfilAcesso);
                myCmd.Parameters.AddWithValue("@v4", f.login.idLogin);
                myCmd.ExecuteNonQuery();
                //login
                cmd = new SqlCommand("UPDATE login SET usuario = @v1,senha = @v2,perfilAcesso = @v3 WHERE idLogin = @v4 ", con);
                cmd.Parameters.AddWithValue("@v1", f.login.usuario);
                cmd.Parameters.AddWithValue("@v2", f.login.senha);
                cmd.Parameters.AddWithValue("@v3", f.login.perfilAcesso);
                cmd.Parameters.AddWithValue("@v4", f.login.idLogin);
                cmd.ExecuteNonQuery();


                //gravar o endereço
                myCmd = new MySqlCommand("UPDATE endereco SET logradouro = @v1, numero = @v2, complemento = @v3, bairro = @v4, cidade = @v5, uf = @v6, cep = @v7,municipio = @v8, zona = @v9 WHERE idEndereco = @v10", myCon);
                myCmd.Parameters.AddWithValue("@v1", f.endereco.longradouro);
                myCmd.Parameters.AddWithValue("@v2", f.endereco.numero);
                myCmd.Parameters.AddWithValue("@v3", f.endereco.complemento);
                myCmd.Parameters.AddWithValue("@v4", f.endereco.bairro);
                myCmd.Parameters.AddWithValue("@v5", f.endereco.cidade);
                myCmd.Parameters.AddWithValue("@v6", f.endereco.uf);
                myCmd.Parameters.AddWithValue("@v7", f.endereco.cep);
                myCmd.Parameters.AddWithValue("@v8", f.endereco.municipio);
                myCmd.Parameters.AddWithValue("@v9", f.endereco.zona);
                myCmd.Parameters.AddWithValue("@v10", f.endereco.idEndereco);
                myCmd.ExecuteNonQuery();
                //gravar o endereço
                cmd = new SqlCommand("UPDATE endereco SET logradouro = @v1, numero = @v2, complemento = @v3, bairro = @v4, cidade = @v5, uf = @v6, cep = @v7,municipio = @v8, zona = @v9 WHERE idEndereco = @v10", con);
                cmd.Parameters.AddWithValue("@v1", f.endereco.longradouro);
                cmd.Parameters.AddWithValue("@v2", f.endereco.numero);
                cmd.Parameters.AddWithValue("@v3", f.endereco.complemento);
                cmd.Parameters.AddWithValue("@v4", f.endereco.bairro);
                cmd.Parameters.AddWithValue("@v5", f.endereco.cidade);
                cmd.Parameters.AddWithValue("@v6", f.endereco.uf);
                cmd.Parameters.AddWithValue("@v7", f.endereco.cep);
                cmd.Parameters.AddWithValue("@v8", f.endereco.municipio);
                cmd.Parameters.AddWithValue("@v9", f.endereco.zona);
                cmd.Parameters.AddWithValue("@v10", f.endereco.idEndereco);
                cmd.ExecuteNonQuery();


                //Pessoa
                myCmd = new MySqlCommand("UPDATE pessoa SET nomeP = @v1, dataNascimento= @v2, sexo = @v3, naturalidade = @v4, nacionalidade = @v5, nomePai = @v6, nomeMae = @v7, etnia = @v8, estadoCivil = @v9, nivelEscolaridade = @v10, necessidadeEsp = @v11 WHERE idPessoa = @v12", myCon);
                myCmd.Parameters.AddWithValue("@v1", f.nome);
                myCmd.Parameters.AddWithValue("@v2", f.dataNascimento);
                myCmd.Parameters.AddWithValue("@v3", f.sexo);
                myCmd.Parameters.AddWithValue("@v4", f.naturalidade);
                myCmd.Parameters.AddWithValue("@v5", f.nacionalidade);
                myCmd.Parameters.AddWithValue("@v6", f.nomePai);
                myCmd.Parameters.AddWithValue("@v7", f.nomeMae);
                myCmd.Parameters.AddWithValue("@v8", f.etnia);
                myCmd.Parameters.AddWithValue("@v9", f.estadoCivil);
                myCmd.Parameters.AddWithValue("@v10", f.nivelEscolaridade);
                myCmd.Parameters.AddWithValue("@v11", f.necessidadeEsp);
                myCmd.Parameters.AddWithValue("@v12", f.idPessoa);
                myCmd.ExecuteNonQuery();
                //Pessoa
                cmd = new SqlCommand("UPDATE pessoa SET nomeP = @v1, dataNascimento= @v2, sexo = @v3, naturalidade = @v4, nacionalidade = @v5, nomePai = @v6, nomeMae = @v7, etnia = @v8, estadoCivil = @v9, nivelEscolaridade = @v10, necessidadeEsp = @v11 WHERE idPessoa = @v12", con);
                cmd.Parameters.AddWithValue("@v1", f.nome);
                cmd.Parameters.AddWithValue("@v2", f.dataNascimento);
                cmd.Parameters.AddWithValue("@v3", f.sexo);
                cmd.Parameters.AddWithValue("@v4", f.naturalidade);
                cmd.Parameters.AddWithValue("@v5", f.nacionalidade);
                cmd.Parameters.AddWithValue("@v6", f.nomePai);
                cmd.Parameters.AddWithValue("@v7", f.nomeMae);
                cmd.Parameters.AddWithValue("@v8", f.etnia);
                cmd.Parameters.AddWithValue("@v9", f.estadoCivil);
                cmd.Parameters.AddWithValue("@v10", f.nivelEscolaridade);
                cmd.Parameters.AddWithValue("@v11", f.necessidadeEsp);
                cmd.Parameters.AddWithValue("@v12", f.idPessoa);
                cmd.ExecuteNonQuery();


                //gravar o funcionario junto com pessoa, código precisa ser melhorado, mas para ter um noção tá valendo
                myCmd = new MySqlCommand("UPDATE funcionario SET situacao = @v1,idCargo = @v2 WHERE idFuncionario = @v3", myCon);
                myCmd.Parameters.AddWithValue("@v1", f.situacao);
                myCmd.Parameters.AddWithValue("@v2", f.cargo.idCargo);
                myCmd.Parameters.AddWithValue("@v3", f.idFuncionario);
                myCmd.ExecuteNonQuery();
                //gravar o funcionario junto com pessoa, código precisa ser melhorado, mas para ter um noção tá valendo
                cmd = new SqlCommand("UPDATE funcionario SET situacao = @v1,idCargo = @v2 WHERE idFuncionario = @v3", con);
                cmd.Parameters.AddWithValue("@v1", f.situacao);
                cmd.Parameters.AddWithValue("@v2", f.cargo.idCargo);
                cmd.Parameters.AddWithValue("@v3", f.idFuncionario);
                cmd.ExecuteNonQuery();


            }
            finally
            {
                FecharConexao();
            }
        }

    }
}