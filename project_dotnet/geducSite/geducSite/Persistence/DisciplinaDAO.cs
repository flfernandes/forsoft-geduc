﻿using geducSite.Context;
using geducSite.Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace geducSite.Persistence
{
    public class DisciplinaDAO : Conexao
    {
        public void Cadastrar(Disciplina d) 
        {
            try
            {
                AbrirConexao();

              
                /*************************************************************************************************
                *
                * 
                * 
                * 
                * 
                *                       My Sql
                *
                * 
                * 
                * 
                * 
                **************************************************************************************************/
                myCmd = new MySqlCommand("INSERT INTO disciplina (nomeD, codigoD,cargaHoraria) VALUES(@v1,@v2,@v3) ", myCon);
                myCmd.Parameters.AddWithValue("@v1", d.nome);
                myCmd.Parameters.AddWithValue("@v2", d.codigo);
                myCmd.Parameters.AddWithValue("@v3", d.cargaHoraria);
                myCmd.ExecuteNonQuery();

                /*************************************************************************************************
              *
              * 
              * 
              * 
              * 
              *                       sql server
              *
              * 
              * 
              * 
              * 
              **************************************************************************************************/

                cmd = new SqlCommand("INSERT INTO disciplina (nomeD, codigoD,cargaHoraria) VALUES(@v1,@v2,@v3) ", con);
                cmd.Parameters.AddWithValue("@v1", d.nome);
                cmd.Parameters.AddWithValue("@v2", d.codigo);
                cmd.Parameters.AddWithValue("@v3", d.cargaHoraria);
                cmd.ExecuteNonQuery();


            }
            finally
            {
                FecharConexao();
            }
        }

        public void Deletar(Disciplina d)
        {
            try
            {
                AbrirConexao();

                /*************************************************************************************************
                *
                * 
                * 
                * 
                * 
                *                       My Sql
                *
                * 
                * 
                * 
                * 
                **************************************************************************************************/

                myCmd = new MySqlCommand("DELETE FROM disciplina WHERE idDisciplina = @v1 ", myCon);
                myCmd.Parameters.AddWithValue("@v1", d.idDisciplina);
                myCmd.ExecuteNonQuery();

                /*************************************************************************************************
                *
                * 
                * 
                * 
                * 
                *                       Sql Server
                *
                * 
                * 
                * 
                * 
                **************************************************************************************************/
                cmd = new SqlCommand("DELETE FROM disciplina WHERE idDisciplina = @v1 ", con);
                cmd.Parameters.AddWithValue("@v1", d.idDisciplina);
                cmd.ExecuteNonQuery();

            }
            finally
            {
                FecharConexao();
            }
        }

        public void Atualizar(Disciplina d)
        {
            try
            {
                AbrirConexao();

                /*************************************************************************************************
                *
                * 
                * 
                * 
                * 
                *                       My Sql
                *
                * 
                * 
                * 
                * 
                **************************************************************************************************/
                myCmd = new MySqlCommand("UPDATE disciplina SET nomeD = @v1, codigoD = @v2 ,cargaHoraria = @v3 WHERE idDisciplina = @v4  ", myCon);
                myCmd.Parameters.AddWithValue("@v1", d.nome);
                myCmd.Parameters.AddWithValue("@v2", d.codigo);
                myCmd.Parameters.AddWithValue("@v3", d.cargaHoraria);
                myCmd.Parameters.AddWithValue("@v4", d.idDisciplina);
                myCmd.ExecuteNonQuery();

                /*************************************************************************************************
               *
               * 
               * 
               * 
               * 
               *                       Sql Server
               *
               * 
               * 
               * 
               * 
               **************************************************************************************************/
                cmd = new SqlCommand("UPDATE disciplina SET nomeD = @v1, codigoD = @v2 ,cargaHoraria = @v3 WHERE idDisciplina = @v4  ", con);
                cmd.Parameters.AddWithValue("@v1", d.nome);
                cmd.Parameters.AddWithValue("@v2", d.codigo);
                cmd.Parameters.AddWithValue("@v3", d.cargaHoraria);
                cmd.Parameters.AddWithValue("@v4", d.idDisciplina);
                cmd.ExecuteNonQuery();
            }
            finally
            {
                FecharConexao();
            }
        }

        public List<Disciplina> ListarDisciplinas()
        {
            try 
            {
                AbrirConexao();
                cmd = new SqlCommand("SELECT * FROM disciplina ORDER BY nomeD",con);
                dr = cmd.ExecuteReader();

                List<Disciplina> lista = new List<Disciplina>();

                while (dr.Read()) 
                {
                    Disciplina d = new Disciplina();
                    d.idDisciplina = Convert.ToInt32(dr["idDisciplina"]);
                    d.nome = Convert.ToString(dr["nomeD"]);
                    d.codigo = Convert.ToString(dr["codigoD"]);
                    d.cargaHoraria = Convert.ToInt32(dr["cargaHoraria"]);
                    lista.Add(d);
                }
                return lista;
            }
            finally 
            {
                FecharConexao();
            }
        }

        public Disciplina BuscarDisciplina(int id)
        {
            try
            {
                AbrirConexao();

                cmd = new SqlCommand("SELECT * FROM disciplina WHERE idDisciplina = @v1", con);
                cmd.Parameters.AddWithValue("@v1",id);
                dr = cmd.ExecuteReader();

                Disciplina d = new Disciplina();
                
                while (dr.Read())
                {
                    d.idDisciplina = Convert.ToInt32(dr["idDisciplina"]);
                    d.nome = Convert.ToString(dr["nomeD"]);
                    d.codigo = Convert.ToString(dr["codigoD"]);
                    d.cargaHoraria = Convert.ToInt32(dr["cargaHoraria"]);
                }

                return d;
            }
            finally
            {
                FecharConexao();
            }
        }
    }
}