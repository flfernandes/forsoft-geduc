﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using geducSite.Models;
using geducSite.Context;
using System.Data.SqlClient;
using System.Collections.ObjectModel;
using MySql.Data.MySqlClient;

namespace geducSite.Persistence
{
    public class NotaDAO : Conexao
    {
        public void CadastrarNota(Nota n)
        {
            try
            {
                AbrirConexao();
              
                /*************************************************************************************************
                * 
                * 
                * 
                * 
                * 
                *                       MySql
                *
                * 
                * 
                * 
                * 
                **************************************************************************************************/

                myCmd = new MySqlCommand("INSERT INTO nota_aluno (idAluno, idDisciplina, nota, periodo, origem, data) VALUES (@V1, @V2, @V3, @V4, @V5, @V6)", myCon);
                myCmd.Parameters.AddWithValue("@v1", n.aluno.idAluno);
                myCmd.Parameters.AddWithValue("@v2", n.disciplina.idDisciplina);
                myCmd.Parameters.AddWithValue("@v3", n.nota);
                myCmd.Parameters.AddWithValue("@v4", n.periodo);
                myCmd.Parameters.AddWithValue("@v5", n.origem);
                myCmd.Parameters.AddWithValue("@v6", n.data);
                myCmd.ExecuteNonQuery();

                /*************************************************************************************************
              *
              * 
              * 
              * 
              * 
              *                       sql server
              *
              * 
              * 
              * 
              * 
              **************************************************************************************************/
                cmd = new SqlCommand("INSERT INTO nota_aluno (idAluno, idDisciplina, nota, periodo, origem, data) VALUES (@V1, @V2, @V3, @V4, @V5, @V6)", con);
                cmd.Parameters.AddWithValue("@v1", n.aluno.idAluno);
                cmd.Parameters.AddWithValue("@v2", n.disciplina.idDisciplina);
                cmd.Parameters.AddWithValue("@v3", n.nota);
                cmd.Parameters.AddWithValue("@v4", n.periodo);
                cmd.Parameters.AddWithValue("@v5", n.origem);
                cmd.Parameters.AddWithValue("@v6", n.data);
                cmd.ExecuteNonQuery();



            }
            catch
            {

                throw;
            }
            finally
            {
                FecharConexao();
            }
        }

        public void Alterar(Nota n)
        {
            try
            {
                AbrirConexao();
               

                /*************************************************************************************************
                * 
                * 
                * 
                * 
                * 
                *                       MySql
                *
                * 
                * 
                * 
                * 
                **************************************************************************************************/

                myCmd = new MySqlCommand("UPDATE nota_aluno SET nota = @v1, periodo = @v2, origem = @v3, data = @v4 WHERE id = @v5", myCon);

                myCmd.Parameters.AddWithValue("@v1", n.nota);
                myCmd.Parameters.AddWithValue("@v2", n.periodo);
                myCmd.Parameters.AddWithValue("@v3", n.origem);
                myCmd.Parameters.AddWithValue("@v4", n.data);
                myCmd.Parameters.AddWithValue("@v5", n.idNota);

                myCmd.ExecuteNonQuery();

                /*************************************************************************************************
               *
               * 
               * 
               * 
               * 
               *                       sql server
               *
               * 
               * 
               * 
               * 
               **************************************************************************************************/

                cmd = new SqlCommand("UPDATE nota_aluno SET nota = @v1, periodo = @v2, origem = @v3, data = @v4 WHERE id = @v5", con);

                cmd.Parameters.AddWithValue("@v1", n.nota);
                cmd.Parameters.AddWithValue("@v2", n.periodo);
                cmd.Parameters.AddWithValue("@v3", n.origem);
                cmd.Parameters.AddWithValue("@v4", n.data);
                cmd.Parameters.AddWithValue("@v5", n.idNota);

                cmd.ExecuteNonQuery();


            }
            catch
            {

                throw;
            }
            finally
            {
                FecharConexao();
            }
        }

        public IEnumerable<Nota> Listar()
        {
            try
            {
                AbrirConexao();
                cmd = new SqlCommand("select * from nota_aluno n inner join aluno a on a.idAluno = n.idAluno inner join pessoa p on p.idPessoa = a.idPessoa inner join disciplina d on d.idDisciplina = n.idDisciplina", con);
                dr = cmd.ExecuteReader();

                Collection<Nota> lista = new Collection<Nota>();
                while (dr.Read())
                {
                    var nota = new Nota();
                    var aluno = new Aluno();
                    var disciplina = new Disciplina();

                    nota.idNota = Convert.ToInt32(dr["id"]);
                    nota.data = Convert.ToDateTime(dr["data"]);
                    nota.nota = Convert.ToInt64(dr["nota"]);
                    nota.origem = Convert.ToString(dr["origem"]);

                    aluno.idAluno = Convert.ToInt32(dr["idAluno"]);
                    aluno.matricula = Convert.ToString(dr["matricula"]);
                    aluno.nome = Convert.ToString(dr["nomeP"]);
                    aluno.dataNascimento = Convert.ToDateTime(dr["dataNascimento"]);
                    aluno.sexo = Convert.ToString(dr["sexo"]);
                    aluno.naturalidade = Convert.ToString(dr["naturalidade"]);
                    aluno.nacionalidade = Convert.ToString(dr["nacionalidade"]);
                    aluno.nomePai = Convert.ToString(dr["nomePai"]);
                    aluno.nomeMae = Convert.ToString(dr["nomeMae"]);
                    aluno.etnia = Convert.ToString(dr["etnia"]);
                    aluno.estadoCivil = Convert.ToString(dr["estadoCivil"]);
                    aluno.nivelEscolaridade = Convert.ToString(dr["nivelEscolaridade"]);
                    aluno.necessidadeEsp = Convert.ToString(dr["necessidadeEsp"]);
                    aluno.situacao = Convert.ToString(dr["situacao"]);
                    aluno.idPessoa = Convert.ToInt32(dr["idPessoa"]);

                    disciplina.idDisciplina = Convert.ToInt32(dr["idDisciplina"]);
                    disciplina.nome = Convert.ToString(dr["nomeD"]);
                    disciplina.cargaHoraria = Convert.ToInt32(dr["cargaHoraria"]);
                    disciplina.codigo = Convert.ToString(dr["codigoD"]);

                    nota.aluno = aluno;
                    nota.disciplina = disciplina;

                    lista.Add(nota);
                }
                return lista;
            }
            finally
            {
                FecharConexao();
            }
        }

    }
}