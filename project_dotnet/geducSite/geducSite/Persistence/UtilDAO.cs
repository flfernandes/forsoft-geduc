﻿using geducSite.Context;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace geducSite.Persistence
{
    public class UtilDAO : Conexao
    {
        public string pegarPerfilAcessoTela(string pagina)
        {
            try
            {
                AbrirConexao();
                cmd = new SqlCommand("SELECT permissoes FROM telas WHERE app_tela = @v1", con);
                cmd.Parameters.AddWithValue("@v1", pagina);
                string p = Convert.ToString(cmd.ExecuteScalar());

                return p;
            }
            finally
            {
                FecharConexao();
            }
        }

        public geducSite.Models.Login SessionLogin(string idSessao)
        {
            try
            {
                AbrirConexao();

                //pegar id login
                myCmd = new MySqlCommand("select idLogin from sessao where idSessao = @id", myCon);
                myCmd.Parameters.AddWithValue("@id", idSessao);
                int idLogin = Convert.ToInt32(myCmd.ExecuteScalar());

                //pegar login pela id da sessão
                Models.Login lg = new Models.Login();
                cmd = new SqlCommand("select * from login where idLogin = @p", con);
                cmd.Parameters.AddWithValue("@p", idLogin);
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    lg.idLogin = Convert.ToInt32(dr["idLogin"]);
                    lg.perfilAcesso = Convert.ToString(dr["perfilAcesso"]);
                    lg.usuario = Convert.ToString(dr["usuario"]);
                    lg.senha = Convert.ToString(dr["senha"]);
                }

                return lg;
            }
            finally
            {
                FecharConexao();
            }
        }
    }
}