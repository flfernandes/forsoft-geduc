﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using geducSite.Models;
using System.Text;
using System.Security.Cryptography;

namespace geducSite.Models
{
    public class Login
    {
        public int idLogin { get; set; }
        public string usuario { get; set; }
        public string senha { get; set; }
        public string perfilAcesso { get; set; }

        public Pessoa pessoa { get; set; }

    }
}