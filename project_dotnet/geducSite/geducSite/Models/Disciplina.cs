﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace geducSite.Models
{
    public class Disciplina
    {
        public int idDisciplina { get; set; }
        public string nome { get; set; }
        public string codigo { get; set; }
        public int cargaHoraria { get; set; }
    }
}