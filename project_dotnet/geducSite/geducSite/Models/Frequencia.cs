﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace geducSite.Models
{
    public class Frequencia
    {
        public int idFrenquencia { get; set; }
        public string situacao { get; set; }
        public DateTime data { get; set; }

        public Aluno aluno { get; set; }
        public Disciplina disciplina { get; set; }
    }
}