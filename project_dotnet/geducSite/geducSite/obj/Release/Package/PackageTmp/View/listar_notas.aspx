﻿<%@ Page Title="" Language="C#" MasterPageFile="~/View/layout.Master" AutoEventWireup="true" CodeBehind="listar_notas.aspx.cs" Inherits="geducSite.View.listar_notas" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Conteudo" runat="server">
    <br />
    <h3>Lista de notas</h3>
    <br />
    <table class="table table-bordered table-striped table-hover table-heading table-datatable">
            <tr >
                <th>Identificação</th>
                <th>Nome do aluno</th>
                <th>Matricula do aluno</th> 
                <th>Nome da disciplina</th>   
                <th>Codigo da disciplina</th>    
                <th>Período</th>    
                <th>Nota do aluno</th>  
                <th>Origim da nota</th>
                <th>Data da nota</th>
            </tr>
        <%foreach (var lista in ListarNotas())
          {%>
            <tr>
                <td><%: lista.idNota %></></td>
                <td><%: lista.aluno.nome %></></td>
                <td><%: lista.aluno.matricula %></td>
                <td><%: lista.disciplina.nome %></td>
                <td><%: lista.disciplina.codigo %></td>
                <td><%: lista.periodo %></td>
                <td><%: lista.nota %></td>
                <td><%: lista.origem %></td>
                <td><%: lista.data %></td>
            </tr>
            <%} %>

    </table>
</asp:Content>
