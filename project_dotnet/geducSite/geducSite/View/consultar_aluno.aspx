﻿<%@ Page Title="" Language="C#" MasterPageFile="~/View/layout.Master" AutoEventWireup="true" CodeBehind="consultar_aluno.aspx.cs" Inherits="geducSite.View.consultar_responsavel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Conteudo" runat="server">
    <div class="row">
        <div class="col-sm-8">
            <div class="box-content">
                <form id="Formulario" runat="server">

                    <h1>Consultar Aluno</h1>

                    <asp:Label Text="Buscar" runat="server" />

                    <div>
                        <asp:TextBox ID="txtBuscar" runat="server" />
                        <asp:DropDownList ID="ddlTipoDeBusca" CssClass="color0" runat="server">
                            <asp:ListItem Value="2">Matricula</asp:ListItem>
                            <asp:ListItem Value="1">CPF</asp:ListItem>
                            <asp:ListItem Value="3">Nome</asp:ListItem>
                            <asp:ListItem Value="4">Sexo</asp:ListItem>
                            <asp:ListItem Value="5">ID do Aluno</asp:ListItem>
                            <asp:ListItem Value="6">RG</asp:ListItem>
                            <asp:ListItem Value="7">Situação</asp:ListItem>
                        </asp:DropDownList>
                        <asp:Button ID="btnBuscar" runat="server" CssClass="btn btn-primary" Text="Buscar" OnClick="btnBuscar_Click" />
                    </div>

                </form>

                <div id="DivBusca" runat="server" visible="false">

                    <% foreach (var lista in buscarAlunos())
                       { %>
                    <div class="">
                        <h2>Acesso:</h2>

                        <asp:Label runat="server" Text="Usuario:"></asp:Label>
                        <label><%: lista.login.usuario %></label>


                        <asp:Label runat="server" Text="Senha:"></asp:Label>
                        <label><%: lista.login.senha %></label>



                        <asp:Label runat="server" Text="Perfil de Acesso:"></asp:Label>
                        <label><%: lista.login.perfilAcesso %></label>



                        <h2>Dados Pessoais:</h2>


                        <asp:Label runat="server" Text="Nome:"></asp:Label>
                        <label><%: lista.nome %></label>

                        <asp:Label runat="server" Text="Data de Nascimento:"></asp:Label>
                        <label><%: lista.dataNascimento %></label>

                        <asp:Label runat="server" Text="Sexo:"></asp:Label>
                        <label><%: lista.sexo %></label>

                        <asp:Label runat="server" Text="Naturalidade:"></asp:Label>
                        <label><%: lista.naturalidade %></label>

                        <asp:Label runat="server" Text="Nacionalidade:"></asp:Label>
                        <label><%: lista.nacionalidade %></label>

                        <asp:Label runat="server" Text="Nome do Pai:"></asp:Label>
                        <label><%: lista.nomePai %></label>

                        <asp:Label runat="server" Text="Nome da M&atilde;e:"></asp:Label>
                        <label><%: lista.nomeMae %></label>

                        <asp:Label ID="Label30" runat="server" Text="Etnia:"></asp:Label>
                        <label><%: lista.etnia %></label>

                        <asp:Label ID="Label29" runat="server" Text="Estado Civil:"></asp:Label>
                        <label><%: lista.estadoCivil %></label>

                        <asp:Label ID="Label28" runat="server" Text="N&iacute;vel de Escolaridade:"></asp:Label>
                        <label><%: lista.nivelEscolaridade %></label>

                        <asp:Label ID="Label27" runat="server" Text="necessidade especial:"></asp:Label>
                        <label><%: lista.necessidadeEsp %></label>

                        <asp:Label ID="Label25" runat="server" Text="Programa Social:"></asp:Label>
                        <label><%: lista.programaSocial.nomePrograma %></label>

                        <h2 class="#">Acadêmico:</h2>


                        <asp:Label ID="Label26" runat="server" Text="Curso:"></asp:Label>
                        <label><%: lista.curso.nomeC %></label>




                        <asp:Label ID="Label24" runat="server" Text="Situa&ccedil;&atilde;o do Aluno:"></asp:Label>
                        <label><%: lista.situacao %></label>




                        <h2>Documenta&ccedil;ao: </h2>

                        <asp:Label ID="Label23" runat="server" Text="CPF:"></asp:Label>
                        <label><%: lista.documento.cpf %></label>


                        <asp:Label ID="Label22" runat="server" Text="RG:"></asp:Label>
                        <label><%: lista.documento.rg %></label>


                        <asp:Label ID="Label21" runat="server" Text="Data de Expedi&ccedil;&atilde;o:"></asp:Label>
                        <label><%: lista.documento.dataExpedicao %></label>


                        <asp:Label ID="Label20" runat="server" Text="Org&atilde;o Expedidor:"></asp:Label>
                        <label><%: lista.documento.orgaoExpedidor %></label>



                        <p>Certid&atilde;o de Nascimento:</p>

                        <asp:Label ID="Label19" runat="server" Text="N&uacute;mero:"></asp:Label>
                        <label><%: lista.documento.numCertidao %></label>


                        <asp:Label ID="Label18" runat="server" Text="Livro:"></asp:Label>
                        <label><%: lista.documento.livroCertidao %></label>


                        <asp:Label ID="Label17" runat="server" Text="Folha:"></asp:Label>
                        <label><%: lista.documento.folhaCertidao %></label>


                        <asp:Label ID="Label16" runat="server" Text="Data de Emiss&atilde;o:"></asp:Label>
                        <label><%: lista.documento.dataEmiCertidao %></label>



                        <asp:Label ID="Label15" runat="server" Text="T&iacute;tulo de Eleitor:"></asp:Label>
                        <label><%: lista.documento.titEleitor %></label>


                        <asp:Label ID="Label14" runat="server" Text="Certificado de Reservista:"></asp:Label>
                        <label><%: lista.documento.certReservista %></label>



                        <h2>Endere&ccedil;o: </h2>

                        <asp:Label ID="Label13" runat="server" Text="Logradouro:"></asp:Label>
                        <label><%: lista.endereco.longradouro %></label>


                        <asp:Label ID="Label12" runat="server" Text="N&uacute;mero:"></asp:Label>
                        <label><%: lista.endereco.numero %></label>


                        <asp:Label ID="Label11" runat="server" Text="Complemento:"></asp:Label>
                        <label><%: lista.endereco.complemento %></label>


                        <asp:Label ID="Label10" runat="server" Text="Bairro:"></asp:Label>
                        <label><%: lista.endereco.bairro %></label>


                        <asp:Label ID="Label9" runat="server" Text="Cidade:"></asp:Label>
                        <label><%: lista.endereco.cidade %></label>


                        <asp:Label ID="Label8" runat="server" Text="CEP:"></asp:Label>
                        <label><%: lista.endereco.cep %></label>


                        <asp:Label ID="Label7" runat="server" Text="UF:"></asp:Label>
                        <label><%: lista.endereco.uf %></label>


                        <asp:Label ID="Label6" runat="server" Text="Munic&iacute;pio:"></asp:Label>
                        <label><%: lista.endereco.municipio %></label>


                        <asp:Label ID="Label5" runat="server" Text="Zona:"></asp:Label>
                        <label><%: lista.endereco.zona %></label>



                        <h2>Contato: </h2>

                        <asp:Label ID="Label4" runat="server" Text="Telefone:"></asp:Label>
                        <label><%: lista.contato.telefoneFixo %></label>



                        <asp:Label ID="Label3" runat="server" Text="Celular:"></asp:Label>
                        <label><%: lista.contato.telefoneCelular %></label>



                        <asp:Label ID="Label2" runat="server" Text="E-mail:"></asp:Label>
                        <label><%: lista.contato.email %></label>



                        <asp:Label ID="Label1" runat="server" Text="Outros:"></asp:Label>
                        <label><%: lista.contato.outros %></label>

                        <h2>Responsável(eis):</h2>


                        <% } %>

                        <div id="DivBuscaR" runat="server" visible="false">

                            <table class="table table-bordered table-striped table-hover table-heading table-datatable" id="tableR">
                                <tr>
                                    <th>Nome:</th>
                                    <th>Grau de Parentesco:</th>
                                    <th>Responsável:</th>
                                    <th>Telefone:</th>
                                    <th>Celular:</th>
                                    <th>Email:</th>
                                    <th>Cidade:</th>
                                    <th>Bairro:</th>
                                    <th>Logradouro:</th>
                                    <th>Número:</th>
                                    <th>Complemento:</th>
                                    <th>UF:</th>
                                </tr>

                                <% foreach (var listaR in buscarResponsaveis())
                                   { %>

                                <tr>
                                    <td><%: listaR.nome %></td>
                                    <td><%: listaR.grauParentesco %></td>
                                    <td><%: listaR.responsavel %></td>
                                    <td><%: listaR.contato.telefoneFixo %></td>
                                    <td><%: listaR.contato.telefoneCelular %></td>
                                    <td><%: listaR.contato.email %></td>
                                    <td><%: listaR.endereco.cidade %></td>
                                    <td><%: listaR.endereco.bairro %></td>
                                    <td><%: listaR.endereco.longradouro %></td>
                                    <td><%: listaR.endereco.numero %></td>
                                    <td><%: listaR.endereco.complemento %></td>
                                    <td><%: listaR.endereco.uf %></td>
                                </tr>
                                <% } %>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
