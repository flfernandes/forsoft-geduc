﻿<%@ Page Title="" Language="C#" MasterPageFile="~/View/layout.Master" AutoEventWireup="true" CodeBehind="alterar_funcionario.aspx.cs" Inherits="geducSite.View.alterar_funcionario" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Conteudo" runat="server">
    
    <div class="row">
            <div class="col-sm-8">
                <div class="box-content">

    <h1 class="page-header text-center">Funcionário</h1>
    
    <form id="Formulario0" runat="server" class="form-horizontal">
        <fieldset>
            <legend class="text-center"><asp:Label ID="Label1" runat="server" Text="Buscar" /></legend>          
            
            <asp:Label runat="server" ID="lbltxtBuscar" CssClass="col-sm-2 control-label">Buscar</asp:Label>
            <div class="col-sm-4">
                <asp:TextBox ID="txtBuscar" runat="server" CssClass="form-control" />
            </div>
            <div class="col-sm-3">
                <asp:DropDownList ID="ddlTipoDeBusca" runat="server" CssClass="form-control">
                    <asp:ListItem Value="2">Matricula</asp:ListItem>
                    <asp:ListItem Value="1">CPF</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="col-sm-3">
                <asp:Button ID="btnBuscar" runat="server" OnClick="btnBuscar_Click" Text="Preencher" CssClass="btn btn-lg btn-primary"/>
            </div>
        </fieldset>
        
        <fieldset>
            <legend class="text-center">Dados Pessoais</legend>
            <div class="form-group">
                <asp:Label ID="lblUsuario" runat="server" Text="Usuario:" CssClass="col-sm-2 control-label"></asp:Label>
                <div class="col-sm-4">
                    <asp:TextBox ID="txtUsuario" runat="server" CssClass="form-control"></asp:TextBox>
                </div>

                <asp:Label ID="lblSenha" runat="server" Text="Senha:" CssClass="col-sm-2 control-label"></asp:Label>
                <div class="col-sm-4">
                    <asp:TextBox ID="txtSenha" MaxLength="50" runat="server"></asp:TextBox>
                </div>
            </div>
            
            <div class="form-group">
                <asp:Label ID="lblPerfilAcesso" runat="server" Text="Perfil de Acesso:" CssClass="col-sm-2 control-label"></asp:Label>
                <div class="col-sm-4">
                    <asp:DropDownList ID="ddlPerfilAcesso" runat="server" CssClass="form-control">
                        <asp:ListItem>Perfil</asp:ListItem>
                        <asp:ListItem Value="A">Aluno/Responsavel </asp:ListItem>
                        <asp:ListItem Value="B">Professor</asp:ListItem>
                        <asp:ListItem Value="C">Secretaria da instituição </asp:ListItem>
                        <asp:ListItem Value="D">Diretor</asp:ListItem>
                        <asp:ListItem Value="E">Ministério da Educação</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
        </fieldset>
        
        <fieldset>
            <legend class="text-center">Dados Pessoais</legend>

            <div class="form-group">
                <asp:Label ID="lblNomeFuncionario" runat="server" Text="Nome:" CssClass="col-sm-1 control-label"></asp:Label>
                <div class="col-sm-5">
                    <asp:TextBox ID="txtNomeFuncionario" MaxLength="50" runat="server" CssClass="form-control"></asp:TextBox>
                </div>

                <asp:Label ID="lblMatricula" runat="server" Text="Matricula:" CssClass="col-sm-2 control-label"></asp:Label>
                <div class="col-sm-4">
                    <asp:TextBox ID="txtMatricula" MaxLength="50" Enabled="false" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            
            
            <div class="form-group">
                
                <asp:Label ID="lblDataNascimento" runat="server" Text="Data de Nascimento" CssClass="col-sm-2 control-label"></asp:Label>
                <div class="col-sm-4">
                <asp:TextBox ID="txtDataNascimento" MaxLength="50" TextMode="DateTime" runat="server"></asp:TextBox>
                </div>

                <asp:Label ID="lblSexo" runat="server" Text="Sexo" CssClass="col-sm-2 control-label"></asp:Label>
                <div class="col-sm-6">
                        <asp:RadioButtonList ID="rbSexo" runat="server" CssClass="checkbox-inline" >
                            <asp:ListItem Value="Masculino" Text="Masculino" />
                            <asp:ListItem Value="Feminino" Text="Feminino" /> 
                        </asp:RadioButtonList>
                </div>
            </div>

            <div class="form-group">
                <asp:Label ID="lblNaturalidade" runat="server" Text="Naturalidade" CssClass="col-sm-2 control-label"></asp:Label>
                <div class="col-sm-4">
                    <asp:TextBox ID="txtNaturalidade" MaxLength="50" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                
                <asp:Label ID="lblNacionalidade" runat="server" Text="Nacionalidade:" CssClass="col-sm-2 control-label"></asp:Label>
                <div class="col-sm-4">
                    <asp:TextBox ID="txtNacionalidade" MaxLength="50" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>

            <div class="form-group">
                <asp:Label ID="lblNomePai" runat="server" Text="Pai" CssClass="col-sm-2 control-label"></asp:Label>
                <div class="col-sm-4">
                    <asp:TextBox ID="txtNomePai" MaxLength="50" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            
            <div class="form-group">
                <asp:Label ID="lblNomeMae" runat="server" Text="M&atilde;e" CssClass="col-sm-2 control-label"></asp:Label>
                <div class="col-sm-4">
                    <asp:TextBox ID="txtNomeMae" MaxLength="50" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>

            <div class="form-group">
                <asp:Label ID="lblEtnia" runat="server" Text="Etnia:" CssClass="col-sm-2 control-label"></asp:Label>
                <div class="col-sm-4">
                <asp:DropDownList ID="ddlEtnia" runat="server" CssClass="form-control">
                    <asp:ListItem Text="Selecione uma Op&ccedil;&atilde;o" Value="selecione" />
                    <asp:ListItem Text="Branco" Value="branco" />
                    <asp:ListItem Text="Preta" Value="preta" />
                    <asp:ListItem Text="Amarela" Value="amarela" />
                    <asp:ListItem Text="Pardo" Value="pardo" />
                    <asp:ListItem Text="Ind&iacute;gena" Value="pardo" />
                    <asp:ListItem Text="Sem Declara&ccedil;&atilde;o" Value="sem" />
                </asp:DropDownList>
                </div>
            </div>

            <div class="form-group">
                <asp:Label ID="lblEstadoCivil" runat="server" Text="Estado Civil" CssClass="col-sm-2 control-label"></asp:Label>
                <div class="col-sm-4">
                <asp:DropDownList ID="ddlEstadoCivil" runat="server" CssClass="form-control">
                    <asp:ListItem Text="Selecione uma Op&ccedil;&atilde;o" Value="selecione" />
                    <asp:ListItem Text="Solteiro(a)" Value="solteiro" />
                    <asp:ListItem Text="Casado(a)" Value="casado" />
                    <asp:ListItem Text="Divorciado(a)" Value="divorciado" />
                    <asp:ListItem Text="Vi&uacute;vo (a)" Value="viuvo" />
                </asp:DropDownList>
                </div>
            </div>
            
            
            <div class="form-group">
            <asp:Label ID="lblEscolaridade" runat="server" Text="N&iacute;vel de Escolaridade::"></asp:Label>
                <asp:DropDownList ID="ddlEscolaridade" runat="server" CssClass="">
                    <asp:ListItem Text="Selecione uma Op&ccedil;&atilde;o" Value="selecione" />
                    <asp:ListItem Text="Superior" Value="superior" />
                    <asp:ListItem Text="Superior Incompleto" Value="superioin" />
                    <asp:ListItem Text="T&eacute;cnico" Value="tecnico" />
                    <asp:ListItem Text="T&eacute;cnico Incompleto (a)" Value="tecnicoin" />
                    <asp:ListItem Text="M&eacute;dio" Value="medio" />
                    <asp:ListItem Text="Vi&M&eacute;dio Incompleto" Value="medioin" />
                    <asp:ListItem Text="Fundamental" Value="fundamental" />
                    <asp:ListItem Text="Fundamental Incompleto" Value="fundamentalin" />
                </asp:DropDownList>
            </div>

            <div class="form-group">
                <asp:Label ID="lblNecessidadeEspecial" runat="server" Text="Possui alguma necessidade especial" CssClass="col-sm-2 control-label"></asp:Label>           
                <div class="col-sm-6">
                        <asp:RadioButtonList ID="rbNecessidadeEspecial"  CssClass="checkbox-inline" runat="server">
                            <asp:ListItem Value="Sim" Text="Sim" />
                            <asp:ListItem Value="Não" Text="Não" /> 
                        </asp:RadioButtonList>
                </div>
            </div>
        </fieldset>
        
        <fieldset>
            <legend class="text-center">Funcion&aacute;rio</legend>

            <asp:Label ID="Cargo" runat="server" Text="Cargo:" CssClass="col-sm-2 control-label"></asp:Label>
            <div class="col-sm-4">
                <asp:DropDownList ID="ddlCargos" runat="server" CssClass="form-control"></asp:DropDownList>
            </div>

            <asp:Label ID="lblSituacao" runat="server" Text="Situa&ccedil;&atilde;o:" CssClass="col-sm-2 control-label"></asp:Label>
            <div class="col-sm-4">
                <asp:DropDownList ID="ddlSituacao" runat="server" CssClass="form-control">
                    <asp:ListItem Text="Selecione uma Op&ccedil;&atilde;o" Value="selecione" />
                    <asp:ListItem Text="Ativo" Value="ativo" />
                    <asp:ListItem Text="Desativo" Value="inativo" />
                </asp:DropDownList>
            </div>
        </fieldset>       
        
        <fieldset>
            <legend class="text-center">Documenta&ccedil;ao</legend>
            
            <div class="form-group">
                <asp:Label ID="lblCpf" runat="server" Text="CPF" CssClass="col-sm-2 control-label"></asp:Label>
                <div class="col-sm-4">
                    <asp:TextBox ID="txtCpf" runat="server"></asp:TextBox>
                </div>
            </div>

            <div class="form-group">
                <asp:Label ID="lblRg" runat="server" Text="RG:" CssClass="col-sm-1 control-label"></asp:Label>
                <div class="col-sm-3">
                <asp:TextBox ID="txtrg" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                
                <asp:Label ID="lblExpedido" runat="server" Text="Data de Expedi&ccedil;&atilde;o:" CssClass="col-sm-1 control-label"></asp:Label>
                <div class="col-sm-3">
                <asp:TextBox ID="txtExpedido" TextMode="DateTime" runat="server" CssClass="form-control"></asp:TextBox>
                </div>

                <asp:Label ID="lblOrgao" runat="server" Text="Org&atilde;o Expedidor:" CssClass="col-sm-1 control-label"></asp:Label>
                <div class="col-sm-3">
                <asp:TextBox ID="txtOrgao" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>            
        </fieldset>

        <fieldset>
            <legend class="text-center">Certid&atilde;o de Nascimento:</legend>
            
            <div class="form-group">
            <asp:Label ID="lblNumCertidaoNascimento" runat="server" Text="N&uacute;mero:" CssClass="col-sm-2 control-label"></asp:Label>
            <div class="col-sm-4">
            <asp:TextBox ID="txtNumCertificadoNascimento" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
            <asp:Label ID="lblLivroCertidaoNascimento" runat="server" Text="Livro" CssClass="col-sm-2 control-label"></asp:Label>
            <div class="col-sm-4">
            <asp:TextBox ID="txtLivroCertidaoNascimento" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
            </div>

            <div class="form-group">
            <asp:Label ID="lblFolhaCertidaoNascimento" runat="server" Text="Folha:" CssClass="col-sm-2 control-label"></asp:Label>
            <div class="col-sm-4">
            <asp:TextBox ID="txtFolhaCertidaoNascimento" runat="server" CssClass="form-control"></asp:TextBox>
            </div>

            <asp:Label ID="lblDataCertidaoNascimento" runat="server" Text="Data de Emiss&atilde;o:" CssClass="col-sm-2 control-label"></asp:Label>
            <div class="col-sm-4">
            <asp:TextBox ID="txtDataCertidaoNascimento" TextMode="DateTime"  runat="server" CssClass="form-control"></asp:TextBox>
            </div>

            <div class="form-group">
            <asp:Label ID="lblTituloEleitor" runat="server" Text="T&iacute;tulo de Eleitor:" CssClass="col-sm-2 control-label"></asp:Label>
            <div class="col-sm-4">
            <asp:TextBox ID="txtTituloEleito" runat="server" CssClass="form-control"></asp:TextBox>
            </div>

            <asp:Label ID="lblCertificadoReservista" runat="server" Text="Certificado de Reservista:" CssClass="col-sm-2 control-label"></asp:Label>
            <div class="col-sm-4">
            <asp:TextBox ID="txtCertificadoReservista" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
            </div>
        </fieldset>
        
        <fieldset>
            <legend>Endere&ccedil;o </legend>
            
            <div class="form-group">
            <asp:Label ID="lblLogradouro" runat="server" Text="Logradouro:" CssClass=""></asp:Label>
            <asp:TextBox ID="txtLogradouro" runat="server" CssClass=""></asp:TextBox>
            </div>
            
            <div class="form-group">
            <asp:Label ID="lblNumero" runat="server" Text="N&uacute;mero:" CssClass=""></asp:Label>
            <asp:TextBox ID="txtNumero" runat="server" CssClass=""></asp:TextBox>
            
            <asp:Label ID="lblComplemento" runat="server" Text="Complemento:" CssClass=""></asp:Label>
            <asp:TextBox ID="txtComplemento" runat="server" CssClass=""></asp:TextBox>
            </div>

            <div class="form-group">
            <asp:Label ID="lblBairro" runat="server" Text="Bairro:" CssClass=""></asp:Label>
            <asp:TextBox ID="txtBairro" runat="server" CssClass=""></asp:TextBox>
            
            <asp:Label ID="lblCidade" runat="server" Text="Cidade:" CssClass=""></asp:Label>
            <asp:TextBox ID="txtCidade" runat="server" CssClass=""></asp:TextBox>            
            </div>

            <div class="form-group">
            <asp:Label ID="lblCEP" runat="server" Text="CEP:" CssClass=""></asp:Label>
            <asp:TextBox ID="txtCEP" runat="server" CssClass=""></asp:TextBox>
            
            <asp:Label ID="lbl" runat="server" Text="UF:" CssClass=""></asp:Label>
            <asp:DropDownList ID="ddlUF" runat="server" CssClass="">
                <asp:ListItem Value=" ">  </asp:ListItem>
                <asp:ListItem Value="1">AC</asp:ListItem>
                <asp:ListItem Value="2">AL</asp:ListItem>
                <asp:ListItem Value="3">AP</asp:ListItem>
                <asp:ListItem Value="4">AM</asp:ListItem>
                <asp:ListItem Value="5">BA</asp:ListItem>
                <asp:ListItem Value="6">CE</asp:ListItem>
                <asp:ListItem Value="7">DF</asp:ListItem>
                <asp:ListItem Value="8">ES</asp:ListItem>
                <asp:ListItem Value="9">GO</asp:ListItem>
                <asp:ListItem Value="10">MA</asp:ListItem>
                <asp:ListItem Value="11">MT</asp:ListItem>
                <asp:ListItem Value="12">MS</asp:ListItem>
                <asp:ListItem Value="13">MG</asp:ListItem>
                <asp:ListItem Value="14">PA</asp:ListItem>
                <asp:ListItem Value="15">PB</asp:ListItem>
                <asp:ListItem Value="16">PR</asp:ListItem>
                <asp:ListItem Value="17">PE</asp:ListItem>
                <asp:ListItem Value="18">PI</asp:ListItem>
                <asp:ListItem Value="19">RJ</asp:ListItem>
                <asp:ListItem Value="20">RN</asp:ListItem>
                <asp:ListItem Value="21">RS</asp:ListItem>
                <asp:ListItem Value="22">RO</asp:ListItem>
                <asp:ListItem Value="23">RR</asp:ListItem>
                <asp:ListItem Value="24">SC</asp:ListItem>
                <asp:ListItem Value="25">SP</asp:ListItem>
                <asp:ListItem Value="26">SE</asp:ListItem>
                <asp:ListItem Value="27">TO</asp:ListItem>
            </asp:DropDownList>

            </div>

            <div class="form-group">

            <asp:Label ID="lblMunicipio" runat="server" Text="Munic&iacute;pio:" CssClass=""></asp:Label>
            <asp:TextBox ID="txtMunicipio" runat="server" CssClass=""></asp:TextBox>
            
            <asp:Label ID="lblZona" runat="server" Text="Zona:" CssClass=""></asp:Label>
            <asp:TextBox ID="txtZona" runat="server" CssClass=""></asp:TextBox>
            </div>
        </fieldset>

        <fieldset>
            <legend class="text-center">Contato</legend>

            <div class="form-group">
                <asp:Label ID="lblTelefone" runat="server" Text="Telefone:" CssClass=""></asp:Label>
                <asp:TextBox ID="txtTelefone" runat="server" CssClass=""></asp:TextBox>
            </div>
            
            <div class="form-group">
                <asp:Label ID="lblCelular" runat="server" Text="Celular:" CssClass=""></asp:Label>
                <asp:TextBox ID="txtCelular" runat="server" CssClass=""></asp:TextBox>
            </div>
            
            <div class="form-group">
                <asp:Label ID="lblEmail" runat="server" Text="E-mail:" CssClass=""></asp:Label>
                <asp:TextBox ID="txtEmail" runat="server" CssClass=""></asp:TextBox>
            </div>
            
            <div class="form-group">
                <asp:Label ID="lblOutros" runat="server" Text="Outros:" CssClass=""></asp:Label>
                <asp:TextBox ID="txtOutros" runat="server" CssClass=""></asp:TextBox>
            </div>
        </fieldset>
        
        <fieldset>
            <legend class="text-center">Programa Social</legend>
        
            <asp:Label ID="lblProgramaSocial" runat="server" Text="Participa de algum programa social" CssClass=""></asp:Label>
            <asp:RadioButton ID="rbProgramaSocialN" runat="server" GroupName="ProgramaSocial" Text="N&atilde;o" CssClass=""/>
            <asp:RadioButton ID="rbProgramaSocialS" runat="server" GroupName="ProgramaSocial" Text="Sim" CssClass=""/>
            <asp:DropDownList ID="ddlProgramaSocial" runat="server" CssClass=""/>
        </fieldset>
        
        <asp:Button ID="btnAlterar" runat="server" Text="Alterar" OnClick="btnAlterar_Click" />
        
        
    </form>
    </div>
    </div>
    </div>
</asp:Content>
