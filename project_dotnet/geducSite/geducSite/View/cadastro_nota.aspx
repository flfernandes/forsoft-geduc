﻿<%@ Page Title="" Language="C#" MasterPageFile="~/View/layout.Master" AutoEventWireup="true" CodeBehind="cadastro_nota.aspx.cs" Inherits="geducSite.View.cadastro_nota" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Conteudo" runat="server">
    <div class="row">
            <div class="col-sm-8">
                <div class="box-content">
    
        <form id="frmNota" runat="server" class="form-horizontal">
            <fieldset>
            <legend class="pageheader text-center">Cadastrar Notas</legend>

                <div class="form-group">
                    <asp:Label CssClass="col-sm-2 control-label" ID="lblAluno" runat="server" Text="Matricula" />
                    <div class="col-sm-4">
                        <asp:DropDownList ID="ddlAluno" runat="server" CssClass="form-control" />
                    </div>

                    <asp:Label CssClass="col-sm-2 control-label" ID="lblDisciplina" runat="server" Text="Disciplina" />
                    <div class="col-sm-4">
                        <asp:DropDownList ID="ddlDisciplina" runat="server" CssClass="form-control" />
                    </div>
                </div>
                
                <div class="form-group">
                <asp:Label CssClass="col-sm-2 control-label" ID="lblDate" runat="server" Text="Data" />
                <div class="col-sm-4">
                <asp:TextBox ID="txtDate" runat="server" required="required" type="date" CssClass="form-control" />
                </div>
                    <asp:Label CssClass="col-sm-2 control-label" ID="lblPeriodo" runat="server" Text="Periodo" />
                <div class="col-sm-4">
                <asp:TextBox ID="txtPeriodo" runat="server" CssClass="form-control" />
                </div>
                </div>
                
                <div class="form-group">
                <asp:Label ID="lblNota" runat="server" Text="Nota" CssClass="col-sm-2 control-label" />
                <div class="col-sm-4">
                <asp:TextBox ID="txtNota" runat="server" CssClass="form-control" />
                </div>
                </div>

                <div class="form-group">
                <asp:Label CssClass="col-sm-2 control-label" ID="lblOrigem" runat="server" Text="Origem" />
                <div class="col-sm-4">
                <asp:TextBox ID="txtOrigem" runat="server" CssClass="form-control"/>
                </div>
                </div>
                
            
            <p><asp:Label ID="lblMensagem" runat="server" /></p>    
                
                
                
            
            
            <asp:Button ID="btnAdicionarNota" class="#" runat="server" Text="Adicionar Nota" OnClick="btnAdicionarNota_Click" CssClass="btn btn-lg btn-primary pull-right"/>
                </fieldset>
        </form>
</div>
</div>
</div>
</asp:Content>
