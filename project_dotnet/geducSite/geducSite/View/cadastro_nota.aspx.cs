﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using geducSite.Persistence;
using geducSite.Models;

namespace geducSite.View
{
    public partial class cadastro_nota : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            geducSite.Models.Login login = (geducSite.Models.Login)Session["login"];

            if (login == null)
            {
                if (Request.Cookies["login"] != null)
                {
                    HttpCookie coockie = Request.Cookies["login"];
                    string ck = coockie.Value.ToString();

                    Session["login"] = new UtilDAO().SessionLogin(ck);
                }
                else
                {
                    Response.Redirect("http://www.projetogeduc.com.br:8080/geduc/login.jsp");
                }
            }
            else if (Util.ValidarAcesso(login.perfilAcesso, "cadastro_nota.aspx") == false)
            {
                Response.Redirect("http://www.projetojeduc.com.br:8080/geduc/nao_pode_acessar.jsp");
            }
            //
            //
            //
            //fim session loginss
            //
            //
            //
            //

            if (!IsPostBack)
            {
                carregarListas();
            }
        }

        private void carregarListas()
        {
            lblMensagem.Text = String.Empty;
            String[] campos = new String[] { txtNota.Text, txtPeriodo.Text, txtPeriodo.Text, ddlAluno.SelectedValue, ddlDisciplina.SelectedValue };
            if (!Validador.seAlgumVazio(campos))
            {
                if (Validar.seSomenteNumero(txtNota.Text) && Validar.seData(txtDate.Text))
                {
                    try
                    {
                        Nota n = new Nota();
                        n.aluno = new Aluno();
                        n.disciplina = new Disciplina();


                        n.aluno.idAluno = Convert.ToInt32(ddlAluno.SelectedValue);
                        n.disciplina.idDisciplina = Convert.ToInt32(ddlDisciplina.SelectedValue);
                        n.data = Convert.ToDateTime(txtDate.Text);
                        n.nota = float.Parse(txtNota.Text);
                        n.periodo = txtPeriodo.Text;
                        n.origem = txtOrigem.Text;

                        NotaDAO dDAO = new NotaDAO();
                        dDAO.CadastrarNota(n);

                        lblMensagem.Text = "A nota foi Adicionada com Sucesso!";
                    }
                    catch (Exception ex)
                    {
                        lblMensagem.Text = ex.Message;
                    }
                }
                else lblMensagem.Text = "Preenchimento de campos inválido";
            }
            else lblMensagem.Text = "Campos obrigatórios não preenchidos.";
 
        }

        protected void btnAdicionarNota_Click(object sender, EventArgs e)
        {
            lblMensagem.Text = String.Empty;

            String[] campos = new String[] { txtNota.Text, txtPeriodo.Text, txtDate.Text };

            if (!Validador.seTudoVazio(campos))
            {
                if (Validar.tamanhoMax(txtPeriodo.Text, 50) && Validar.tamanhoMax(txtOrigem.Text, 200))
                {
                    if (Validar.seSomenteNumero(txtNota.Text) && Validar.seData(txtDate.Text))
                    {
                        try
                        {
                            Nota n = new Nota();
                            n.aluno = new Aluno();
                            n.disciplina = new Disciplina();


                            n.aluno.idAluno = Convert.ToInt32(ddlAluno.SelectedValue);
                            n.disciplina.idDisciplina = Convert.ToInt32(ddlDisciplina.SelectedValue);
                            n.data = Convert.ToDateTime(txtDate.Text);
                            n.nota = float.Parse(txtNota.Text);
                            n.periodo = txtPeriodo.Text;
                            n.origem = txtOrigem.Text;

                            NotaDAO dDAO = new NotaDAO();
                            dDAO.CadastrarNota(n);

                            lblMensagem.Text = "A nota foi Adicionada com Sucesso! ";
                        }
                        catch (Exception ex)
                        {
                            lblMensagem.Text = ex.Message;
                        }
                    }
                    else lblMensagem.Text = "Preenchimento de campos inválido.";
                }
                else lblMensagem.Text = "Ultrapassou o limite máximo de caracteres";
            }
            else lblMensagem.Text = "Nenhum campo preenchido.";
        }
    }
}