﻿using geducSite.Persistence;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using geducSite.Models;
using System.Collections.ObjectModel;

namespace geducSite.View
{
    public partial class cadastro_funcionario : System.Web.UI.Page
    {
        protected IEnumerable<Funcionario> TodosOsFuncionarios;
        protected IEnumerable<ProgramaSocial> TodosOsProgramasSociais;
        protected IEnumerable<Cargo> TodosOsCargos;

        protected void Page_Load(object sender, EventArgs e)
        {
            geducSite.Models.Login login = (geducSite.Models.Login)Session["login"];

            if (login == null)
            {
                if (Request.Cookies["login"] != null)
                {
                    HttpCookie coockie = Request.Cookies["login"];
                    string ck = coockie.Value.ToString();

                    Session["login"] = new UtilDAO().SessionLogin(ck);
                }
                else
                {
                    Response.Redirect("http://www.projetogeduc.com.br:8080/geduc/login.jsp");
                }
            }
            else if (Util.ValidarAcesso(login.perfilAcesso, "cadastro_funcionario.aspx") == false)
            {
                Response.Redirect("http://www.projetojeduc.com.br:8080/geduc/nao_pode_acessar.jsp");
            }
            //fim session login


            TodosOsFuncionarios = new FuncionarioDAO().Listar();
            TodosOsProgramasSociais = new ProgramaSocialDAO().listar();
            TodosOsCargos = new CargoDAO().Listar();

            ddlCargos.DataSource = TodosOsCargos;
            ddlCargos.DataValueField = "idCargo";
            ddlCargos.DataTextField = "cargo";
            ddlCargos.DataBind();

            ddlProgramaSocial.DataSource = TodosOsProgramasSociais;
            ddlProgramaSocial.DataValueField = "idProgramaSocial";
            ddlProgramaSocial.DataTextField = "nomePrograma";
            ddlProgramaSocial.DataBind();
        }

        protected void AddNaSession(Funcionario f)
        {
            if (Session["ListaFuncionarios"] != null)
            {
                var addSession = (Collection<Funcionario>)Session["ListaFuncionarios"];
                addSession.Add(f);
                Session["ListaFuncionarios"] = (IEnumerable<Funcionario>)addSession;
            }
        }

        protected void Cadastrar_Click(object sender, EventArgs e)
        {
            String[] campos = new String[] { txtUsuario.Text, txtSenha.Text, txtNomeFuncionario.Text, txtDataNascimento.Text,
                txtNacionalidade.Text, txtNaturalidade.Text, ddlEscolaridade.SelectedValue, ddlEtnia.SelectedValue, ddlEstadoCivil.SelectedValue,
                txtLogradouro.Text, txtNumero.Text, txtBairro.Text, txtCidade.Text, ddlUF.SelectedValue, txtCEP.Text, txtMunicipio.Text };
            String[] camposSomenteNumero = new String[] { txtCpf.Text, txtCelular.Text, txtTelefone.Text, txtCEP.Text };
            String[] camposSomenteLetras = new String[] { txtNomeFuncionario.Text, txtNaturalidade.Text, txtNacionalidade.Text, txtNomePai.Text,
                txtNomeMae.Text, txtCidade.Text, txtMunicipio.Text };
            String[] camposData = new String[] { txtDataNascimento.Text, txtExpedido.Text, txtDataCertidaoNascimento.Text };

            String[] campos255 = new String[] { txtUsuario.Text, txtSenha.Text, txtMatricula.Text, txtLogradouro.Text, txtBairro.Text };
            String[] campos200 = new String[] { txtNomePai.Text, txtNomeMae.Text, txtNumCertificadoNascimento.Text, txtLivroCertidaoNascimento.Text,
                txtFolhaCertidaoNascimento.Text, txtCertificadoReservista.Text, txtEmail.Text };
            String[] campos100 = new String[] { txtNacionalidade.Text, txtOrgao.Text, txtTituloEleito.Text };
            String[] campos40 = new String[] { txtNumero.Text, txtComplemento.Text, txtCidade.Text, ddlUF.SelectedValue, txtMunicipio.Text,
                txtZona.Text };
            String[] campos30 = new String[] { ddlEscolaridade.SelectedValue, ddlSituacao.SelectedValue };
            String[] campos20 = new String[] { txtNaturalidade.Text, rbSexo.SelectedValue, ddlEtnia.SelectedValue,
                rbNecessidadeEspecial.SelectedValue, ddlEstadoCivil.SelectedValue, txtTelefone.Text, txtCelular.Text };

            if (!Validador.seAlgumVazio(campos))
            {
                if (Validador.tamanhoMax(campos20, 20) && Validador.tamanhoMax(campos200, 200) && Validar.tamanhoMax(txtNomeFuncionario.Text, 150) &&
                    Validador.tamanhoMax(campos100, 100) && Validador.tamanhoMax(campos30, 30) && Validador.tamanhoMax(campos255, 255) &&
                    Validador.tamanhoMax(campos40, 40))
                {
                    if (Validador.seSomenteLetra(camposSomenteLetras) && Validador.seSomenteNumero(camposSomenteNumero) && Validador.seData(camposData)
                    && Validar.seCEP(txtCEP.Text) && Validar.seCPF(txtCpf.Text) && Validar.seEmail(txtEmail.Text))
                    {
                        try
                        {
                            Funcionario f = new Funcionario();
                            f.nome = txtNomeFuncionario.Text;
                            f.matricula = txtMatricula.Text;
                            f.dataNascimento = DateTime.Parse(txtDataNascimento.Text);
                            f.sexo = rbSexo.SelectedValue;
                            f.naturalidade = txtNaturalidade.Text;
                            f.nacionalidade = txtNacionalidade.Text;
                            f.nomePai = txtNomePai.Text;
                            f.nomeMae = txtNomeMae.Text;
                            f.etnia = ddlEtnia.SelectedValue;
                            f.estadoCivil = ddlEstadoCivil.SelectedValue;
                            f.nivelEscolaridade = ddlEscolaridade.SelectedValue;
                            f.necessidadeEsp = rbNecessidadeEspecial.SelectedValue;
                            f.cargo = TodosOsCargos.SingleOrDefault(x => x.idCargo == Convert.ToInt32(ddlCargos.SelectedValue));
                            f.situacao = ddlSituacao.SelectedValue;


                            geducSite.Models.Login lg = new geducSite.Models.Login();
                            lg.usuario = txtUsuario.Text;
                            lg.senha = Criptografia.Encriptar( txtSenha.Text);
                            lg.perfilAcesso = ddlPerfilAcesso.SelectedValue;

                            f.login = lg;

                            Documento d = new Documento();
                            d.cpf = txtCpf.Text;


                            d.rg = txtrg.Text;
                            d.dataExpedicao = DateTime.Parse(txtExpedido.Text);
                            d.orgaoExpedidor = txtExpedido.Text;
                            d.numCertidao = txtNumCertificadoNascimento.Text;
                            d.livroCertidao = txtLivroCertidaoNascimento.Text;
                            d.folhaCertidao = txtFolhaCertidaoNascimento.Text;
                            d.dataEmiCertidao = DateTime.Parse(txtDataCertidaoNascimento.Text);
                            d.titEleitor = txtTituloEleito.Text;
                            d.certReservista = txtCertificadoReservista.Text;

                            f.documento = d;

                            Endereco en = new Endereco();


                            en.longradouro = txtLogradouro.Text;
                            en.numero = txtNumero.Text;
                            en.complemento = txtComplemento.Text;
                            en.bairro = txtBairro.Text;
                            en.cidade = txtCidade.Text;
                            en.cep = txtCEP.Text;
                            en.uf = ddlUF.SelectedValue;
                            en.municipio = txtMunicipio.Text;
                            en.zona = txtZona.Text;

                            f.endereco = en;

                            Contato con = new Contato();

                            con.telefoneFixo = txtTelefone.Text;
                            con.telefoneCelular = txtCelular.Text;
                            con.email = txtEmail.Text;
                            con.outros = txtOutros.Text;

                            f.contato = con;

                            ProgramaSocial ps = new ProgramaSocial();

                            if (rbProgramaSocialS.Checked == true)
                            {
                                f.programaSocial = TodosOsProgramasSociais.SingleOrDefault(x => x.idProgramaSocial == Convert.ToInt32(ddlProgramaSocial.SelectedValue));
                            }
                            else
                            {
                                f.programaSocial = TodosOsProgramasSociais.SingleOrDefault(x => x.idProgramaSocial == 1); //sugiro colocar o primeiro item da tabela programaSocial como nomePrograma="Não Existemte" ou "Não tem"
                            }


                            new FuncionarioDAO().Salvar(f);
                            AddNaSession(f);

                            Limpar();

                            Response.Redirect("cadastro_funcionario.aspx");
                        }
                        catch (Exception erro)
                        {
                            erro.ToString();
                        }
                    }
                    /*else msg.Text = "Preenchimento de campos inválido.";*/
                }
                /*else msg.Text = "Ultrapassou o limite máximo de caracteres";*/
            }
            /*else msg.Text = "Nenhum campo preenchido";*/
        }


        protected void brnListar_Click(object sender, EventArgs e)
        {
            Response.Redirect("listar_funcionario.aspx");
        }

        protected void Limpar()
        {

            txtUsuario.Text = string.Empty;
            txtSenha.Text = string.Empty;
            txtNomeFuncionario.Text = string.Empty;
            txtDataNascimento.Text = string.Empty;
            txtDataCertidaoNascimento.Text = string.Empty;
            rbSexo.SelectedValue = string.Empty;
            txtNaturalidade.Text = string.Empty;
            txtNacionalidade.Text = string.Empty;
            txtNomePai.Text = string.Empty;
            txtNomeMae.Text = string.Empty;
            rbNecessidadeEspecial.SelectedValue = string.Empty;
            txtrg.Text = string.Empty;
            txtExpedido.Text = string.Empty;
            txtOrgao.Text = string.Empty;
            txtNumCertificadoNascimento.Text = string.Empty;
            txtLivroCertidaoNascimento.Text = string.Empty;
            txtFolhaCertidaoNascimento.Text = string.Empty;
            txtDataCertidaoNascimento.Text = string.Empty;
            txtTituloEleito.Text = string.Empty;
            txtCertificadoReservista.Text = string.Empty;
            txtLogradouro.Text = string.Empty;
            txtNumero.Text = string.Empty;
            txtComplemento.Text = string.Empty;
            txtBairro.Text = string.Empty;
            txtCEP.Text = string.Empty;
            txtMunicipio.Text = string.Empty;
            txtZona.Text = string.Empty;
            txtTelefone.Text = string.Empty;
            txtCelular.Text = string.Empty;
            txtEmail.Text = string.Empty;
            txtOutros.Text = string.Empty;
            rbProgramaSocialN.Text = string.Empty;
        }

        protected void btnLimpar_Click(object sender, EventArgs e)
        {
            Limpar();
        }

    }
}