﻿<%@ Page Title="" Language="C#" MasterPageFile="~/View/layout.Master" AutoEventWireup="true" CodeBehind="cadastro_programaSocial.aspx.cs" Inherits="geducSite.View.cadastro_programaSocial" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Conteudo" runat="server">
    <div class="box-content">
        <form id="form1" runat="server">
        
            <!-- Início do Código -->
            <fieldset>
    		<legend>Cadastro de Programa Social
            </legend>
            </fieldset>
            <div>
                <asp:Label ID="lblNome" runat="server" Text="Nome do programa social:   " />
                <asp:TextBox ID="txtNome" runat="server"></asp:TextBox><br /><br />

                <asp:Label ID="lblDescricao" runat="server" Text="Descricao:   " />
                <asp:TextBox ID="txtDescricao" runat="server"></asp:TextBox><br /><br />

                <asp:Label ID="lblAmbito" runat="server" Text="AmbitoAdm   " />
                <asp:TextBox ID="txtAmbito" runat="server"></asp:TextBox><br /><br />
            
                
                <asp:Label ID="msg" runat="server"></asp:Label>
                <br />
                <asp:Button ID="btnLimpar"  runat="server" Text="Limpar" OnClick="btnLimpar_Click" />
                <asp:Button ID="btnCadastrarProgSoci" runat="server" Text="Cadastrar" OnClick="btnCadastrarProgSoci_Click" />
            </div>
            </form>
        </div>
</asp:Content>