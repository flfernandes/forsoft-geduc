﻿<%@ Page Title="" Language="C#" MasterPageFile="~/View/layout.Master" AutoEventWireup="true" CodeBehind="cadastro_responsavel.aspx.cs" Inherits="geducSite.View.cadastro_responsavel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Conteudo" runat="server">

<div class="row">
            <div class="col-sm-8">
                <div class="box-content">
        <h1 class="page-header text-center">Cadastro de Responsável<asp:Label ID="msg" runat="server"></asp:Label></h1>
        <form id="form1" runat="server" class="form-horizontal">
        
            <fieldset>
                <legend class="text-center">Selecione o Aluno</legend>
                    <asp:TextBox  ID="txtBuscar" runat="server" CssClass="col-sm-4" />
                       <div class="col-sm-6">
                    <asp:DropDownList ID="ddlTipoDeBusca" CssClass="form-control" runat="server">
                        <asp:ListItem Value="1">Matricula</asp:ListItem>
                        <asp:ListItem Value="2">CPF</asp:ListItem>
                        <asp:ListItem Value="3">RG</asp:ListItem>
                    </asp:DropDownList>
                    <asp:Button ID="btnBuscar" runat="server"  Text="Buscar" OnClick="btnBuscar_Click" CssClass="btn btn-lg btn-primary"/>
                   </div>
                
                <div class="form-group">
                    <asp:Label ID="aluno" Text="Aluno: " runat="server" Visible="false" CssClass="col-sm-2 control-label"/>
                    <asp:Label ID="lblAluno" runat="server" />
                    <asp:Label ID="lblIdAluno" runat="server" Visible="false"/>
                </div>
            </fieldset>
            
            <fieldset>                
                <legend class="page-header text-center">Login</legend>
                    <asp:Label ID="lblNomeUsuario" runat="server" Text="Usu&aacute;rio:" CssClass="col-sm-2 control-label"></asp:Label>
                    <div class="col-sm-4">
                        <asp:TextBox  ID="txtUsuario" name="lblNomeUsuario" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
        								
        		    <asp:Label ID="lblSenha" runat="server" Text="Senha:" CssClass="col-sm-2 control-label"></asp:Label>
                <div class="col-sm-4">
                    <asp:TextBox  ID="txtSenha" name="lblSenha" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
               
            </fieldset>			
    		
            <fieldset>
    			<legend class="text-center">Dados Pessoais</legend>
                
                <div class="form-group">
                    <asp:Label CssClass="col-sm-2 control-label" ID="lblGrauParentesco" Text="Grau de Parentesco" runat="server"/>
                    <div class="col-sm-4">
                        <asp:TextBox CssClass="form-control" ID="txtParentesco" runat="server" />
                    </div>

                    <asp:Label ID="lblNomeAluno" runat="server" Text="Nome" CssClass="col-sm-2 control-label" ></asp:Label>
                    <div class="col-sm-4">
                        <asp:TextBox CssClass="form-control" ID="txtNomeAluno" name="lblNomeAluno" runat="server"></asp:TextBox>
                    </div>
                </div>

                <div class="form-group">
                <asp:Label ID="lblResponsavel" Text="É responsável pelo Aluno?" runat="server" CssClass="col-sm-3 control-label"></asp:Label>                
                <div class="col-sm-3">
                <asp:DropDownList ID="ddlResponsavel" runat="server" CssClass="form-control">
                    <asp:ListItem Value="Sim">Sim</asp:ListItem>
                    <asp:ListItem Value="Não">Não</asp:ListItem>
                    <asp:ListItem Value="Apenas parente">Apenas parente</asp:ListItem>
                </asp:DropDownList>                
                </div>

                    <asp:Label CssClass="col-sm-2 control-label" ID="lblDataNascimento" runat="server" Text="Data de Nascimento" ></asp:Label>
                <div class="col-sm-4">
                <asp:TextBox CssClass="form-control" ID="txtDataNascimento" name="lblDataNascimento" runat="server"  type="date" />
                </div>
                    </div>

                <div class="form-group">
            	<asp:Label CssClass="col-sm-2 control-label" ID="lblSexo" runat="server" Text="Sexo:"></asp:Label>	
                <div class="col-sm-4">
                <asp:RadioButtonList ID="rbSexo" runat="server">
                    <asp:ListItem Value="Masculino" Text="Masculino" CssClass="radio-inline"/>
                    <asp:ListItem Value="Feminino" Text="Feminino" CssClass="radio-inline" /> 
                </asp:RadioButtonList>	
                </div>

                </div>

                <div class="form-group">

    			<asp:Label ID="lblNaturalidade" runat="server" Text="Naturalidade:" CssClass="col-sm-2 control-label"></asp:Label>
                <div class="col-sm-4">
                <asp:TextBox CssClass="form-control" ID="txtNaturalidade" name="lblNaturalidade" runat="server" ></asp:TextBox>
                </div>

                <asp:Label CssClass="col-sm-2 control-label" ID="lblNacionalidade" runat="server" Text="Nacionalidade:" ></asp:Label>
                <div class="col-sm-4">
                <asp:TextBox CssClass="form-control" ID="txtNacionalidade" name="lblNacionalidade" runat="server" ></asp:TextBox>
                </div>

                </div>                                      

                <div class="form-group">

                <asp:Label ID="lblNomePai" runat="server" Text="Pai:" CssClass="col-sm-2 control-label" ></asp:Label>
                <div class="col-sm-4">
                <asp:TextBox CssClass="form-control" ID="txtNomePai" name="lblNomePai" runat="server" ></asp:TextBox>    			
                </div>
                </div>

                <div class="form-group">
                <asp:Label CssClass="col-sm-2 control-label" ID="lblNomeMae" runat="server" Text="Nome da M&atilde;e:" ></asp:Label>
                <div class="col-sm-4">
                <asp:TextBox CssClass="form-control" ID="txtNomeMae" name="lblNomeMae" runat="server" ></asp:TextBox>    
                </div>

                </div>
    			

                <div class="form-group">
    			<asp:Label CssClass="col-sm-2 control-label" ID="lblEtnia" runat="server" Text="Etnia:" ></asp:Label>
                <div class="col-sm-4">
                <asp:DropDownList ID="ddlEtnia" name="lblEtnia" runat="server" CssClass="form-control">
                    <asp:ListItem>Branco(a)</asp:ListItem>
                    <asp:ListItem>Negro(a)</asp:ListItem>
                    <asp:ListItem>Amarelo(a)</asp:ListItem>
                    <asp:ListItem>Pardo</asp:ListItem>
                    <asp:ListItem>Ind&iacute;gena</asp:ListItem>
                    <asp:ListItem>Sem Declara&ccedil;&atilde;o</asp:ListItem>
                </asp:DropDownList>
                </div>

                <asp:Label CssClass="col-sm-2 control-label" ID="lblEstadoCivil" runat="server" Text="Estado Civil:" ></asp:Label>
                <div class="col-sm-4">
                <asp:DropDownList ID="ddlEstadoCivil" name="lblEstadoCivil" runat="server" CssClass="form-control">
                    <asp:ListItem>Solteiro (a)</asp:ListItem>
                    <asp:ListItem>Casado (a)</asp:ListItem>
                    <asp:ListItem>Divorciado (a)</asp:ListItem>
                    <asp:ListItem>Vi&uacute;vo (a)</asp:ListItem>
                    <asp:ListItem>Sem Declara&ccedil;&atilde;o</asp:ListItem>
                </asp:DropDownList>
                </div>
                </div>                                          
                
                <div class="form-group">
                <asp:Label ID="lblEscolaridade" runat="server" Text="N&iacute;vel de Escolaridade:" CssClass="col-sm-2 control-label"></asp:Label>
                <div class="col-sm-4">
                <asp:DropDownList ID="ddlEscolaridade" name="lblEscolaridade" runat="server" CssClass="form-control">
                    <asp:ListItem>Escolha uma op&ccedil;&atilde;o</asp:ListItem>
                    <asp:ListItem>Superior</asp:ListItem>
                    <asp:ListItem>Superior Incompleto</asp:ListItem>
                    <asp:ListItem>T&eacute;cnico</asp:ListItem>
                    <asp:ListItem>T&eacute;cnico Incompleto</asp:ListItem>
                    <asp:ListItem>M&eacute;dio</asp:ListItem>
                    <asp:ListItem>M&eacute;dio Incompleto</asp:ListItem>
                    <asp:ListItem>Fundamental</asp:ListItem>
                    <asp:ListItem>Fundamental Incompleto</asp:ListItem>
                </asp:DropDownList>
                </div>
                </div>
                
                <div class="form-group">
                <asp:Label ID="lblNecessidadeEspecial"  runat="server" Text="Possui alguma necessidade especial?" CssClass="col-sm-3 control-label"></asp:Label>
                <div class="col-sm-3">
                <asp:RadioButtonList ID="rbNecessidadeEspecial"	runat="server">
                    <asp:ListItem Value="Sim" Text="Sim" CssClass="checkbox-inline"/>
                    <asp:ListItem Value="Não" Text="Não" CssClass="checkbox-inline" />
                </asp:RadioButtonList>
                </div>
                </div>
                
                </fieldset>

                <fieldset>
    			<legend class="text-center">Documenta&ccedil;ao</legend>

                <div class="form-group">
    			    <asp:Label CssClass="col-sm-2" ID="lblCPF" runat="server" Text="CPF" ></asp:Label>
                    <div class="col-sm-4">
                        <asp:TextBox CssClass="form-control" ID="txtCPF" name="lblCPF" runat="server" ></asp:TextBox>
                    </div>

                    <asp:Label ID="lblRG" runat="server" Text="RG:" CssClass="col-sm-2 control-label"></asp:Label>
                    <div class="col-sm-4">
                        <asp:TextBox CssClass="form-control" ID="txtRG" name="lblRG" runat="server" ></asp:TextBox>
    			    </div>              
                </div>
                
                <div class="form-group">
    			<asp:Label CssClass="col-sm-3 control-label" ID="lblExpedido" runat="server" Text="Data de Expedi&ccedil;&atilde;o:" ></asp:Label>
                    <div class="col-sm-3">
                        <asp:TextBox CssClass="form-control" ID="txtDataExpedicao" name="lblExpedido" runat="server"  type="date"></asp:TextBox>
                    </div>   			
                
    			<asp:Label CssClass="col-sm-2 control-label" ID="lblOrgao" runat="server" Text="Org&atilde;o Expedidor:" ></asp:Label>
                    <div class="col-sm-4">
                        <asp:TextBox CssClass="form-control" ID="txtOrgao" name="lblOrgao" runat="server" ></asp:TextBox><asp:Label ID="Label19" runat="server" Text="*" class="form-control"/>   
    			    </div>
                </div>

                </fieldset>
                
                <fieldset>
    			<legend class="text-center">Certid&atilde;o de Nascimento:</legend>
                 
                <div class="form-group">
                <asp:Label CssClass="col-sm-2 control-label" ID="lblNumCertidaoNascimento" runat="server" Text="N&uacute;mero:" ></asp:Label>
                    <div class="col-sm-4">
                        <asp:TextBox CssClass="form-control" ID="txtNumCertidaoNascimento" name="lblNumCertidaoNascimento" runat="server" ></asp:TextBox>
                    </div>
    			    <asp:Label CssClass="col-sm-2 control-label" ID="lblLivroCertidaoNascimento" runat="server" Text="Livro:" ></asp:Label>
                    <div class="col-sm-4">
                        <asp:TextBox CssClass="form-control" ID="txtLivroCertidaoNascimento" name="lblLivroCertidaoNascimento" runat="server" ></asp:TextBox>
                    </div>
                </div>
    			

                <div class="form-group">
                
    			<asp:Label CssClass="col-sm-2 control-label" ID="lblFolhaCertidaoNascimento" runat="server" Text="Folha:" ></asp:Label>
                <div class="col-sm-4">
                <asp:TextBox CssClass="form-control" ID="txtFolhaCertidaoNascimento" name="lblFolhaCertidaoNascimento" runat="server" ></asp:TextBox>
                </div>
    			

    			<asp:Label CssClass="col-sm-2 control-label" ID="lblDataCertidaoNascimento" runat="server" Text="Data de Emiss&atilde;o:" ></asp:Label>
                <div class="col-sm-4">
                <asp:TextBox CssClass="form-control" ID="txtDataCertidaoNascimento" name="lblDataCertidaoNascimento" runat="server"  type="date"></asp:TextBox>
                </div>  
                </div>                                        
    			
            								
                <div class="form-group">
                <asp:Label CssClass="col-sm-2 control-label" ID="lblTituloEleitor" runat="server" Text="T&iacute;tulo de Eleitor:" ></asp:Label>
                <div class="col-sm-4">
                <asp:TextBox CssClass="form-control" ID="txtTituloEleitor" name="lblTituloEleitor" runat="server" ></asp:TextBox>   
                </div>
    			
                
                <asp:Label CssClass="col-sm-2 control-label" ID="lblCertificadoReservista" runat="server" Text="Certificado de Reservista:" ></asp:Label>
                <div class="col-sm-4">
                <asp:TextBox CssClass="form-control" ID="txtCertificadoReservista" name="lblCertificadoReservista" runat="server" ></asp:TextBox>
                </div>
                </div>
            </fieldset>

            <fieldset>
    			<legend class="text-center">Endere&ccedil;o</legend>
                
                <div class="form-group">
    			    <asp:Label CssClass="col-sm-2 control-label" ID="lblLogradouro" runat="server" Text="Logradouro" ></asp:Label>
                    <div class="col-sm-6">
                        <asp:TextBox  ID="txtLogradouro" name="lblLogradouro" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>

                <div class="form-group">
                    <asp:Label CssClass="col-sm-2 control-label" ID="lblNumero" runat="server" Text="N&uacute;mero:" ></asp:Label>
                    <div class="col-sm-4">
                        <asp:TextBox  ID="txtNumero" name="lblNumero" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>	    								
    			
                    <asp:Label CssClass="col-sm-2 control-label" ID="lblComplemento" runat="server" Text="Complemento:" ></asp:Label>
                    <div class="col-sm-4">
                        <asp:TextBox  ID="txtComplemento" name="lblComplemento" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>

                <div class="form-group">
    			<asp:Label CssClass="col-sm-2 control-label" ID="lblBairro" runat="server" Text="Bairro:" ></asp:Label>
                <div class="col-sm-4">
                <asp:TextBox  ID="txtBairro" name="lblBairro" runat="server" CssClass="form-control"></asp:TextBox>    	
                </div>	

    			<asp:Label CssClass="col-sm-2 control-label" ID="lblCidade" runat="server" Text="Cidade:" ></asp:Label>
                <div class="col-sm-4">
                <asp:TextBox  ID="txtCidade" name="lblCidade" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                </div>    	


                <div class="form-group">
                <asp:Label ID="lblUF" runat="server" Text="UF:" CssClass="col-sm-2 control-label" ></asp:Label>
                <div class="col-sm-2">
                <asp:DropDownList ID="ddlUF" name="lblUF" runat="server" CssClass="form-control">
                    <asp:ListItem></asp:ListItem>
    				<asp:ListItem>AC</asp:ListItem>
                    <asp:ListItem>AL</asp:ListItem> 
                    <asp:ListItem>AP</asp:ListItem> 
                    <asp:ListItem>AM</asp:ListItem> 
                    <asp:ListItem>BA</asp:ListItem> 
                    <asp:ListItem>CE</asp:ListItem> 
                    <asp:ListItem>DF</asp:ListItem> 
                    <asp:ListItem>ES</asp:ListItem> 
                    <asp:ListItem>GO</asp:ListItem> 
                    <asp:ListItem>MA</asp:ListItem> 
                    <asp:ListItem>MT</asp:ListItem> 
                    <asp:ListItem>MS</asp:ListItem> 
                    <asp:ListItem>MG</asp:ListItem> 
                    <asp:ListItem>PA</asp:ListItem> 
                    <asp:ListItem>PB</asp:ListItem> 
                    <asp:ListItem>PR</asp:ListItem> 
                    <asp:ListItem>PE</asp:ListItem> 
                    <asp:ListItem>PI</asp:ListItem> 
                    <asp:ListItem Selected="True">RJ</asp:ListItem> 
                    <asp:ListItem >RN</asp:ListItem> 
                    <asp:ListItem>RS</asp:ListItem> 
                    <asp:ListItem>RO</asp:ListItem> 
                    <asp:ListItem>RR</asp:ListItem> 
                    <asp:ListItem>SC</asp:ListItem> 
                    <asp:ListItem>SP</asp:ListItem> 
                    <asp:ListItem>SE</asp:ListItem> 
                    <asp:ListItem>TO</asp:ListItem>  	                     
                </asp:DropDownList>
                </div>
                
    											
    			<asp:Label CssClass="col-sm-2 control-label" ID="lblCEP" runat="server" Text="CEP:" ></asp:Label>
                <div class="col-sm-4">
                <asp:TextBox CssClass="form-control" ID="txtCEP" name="lblCEP" runat="server" ></asp:TextBox>
                </div>
                </div>

                <div class="form-group">

                <asp:Label CssClass="col-sm-2 control-label" ID="lblMunicipio" runat="server" Text="Munic&iacute;pio:" ></asp:Label>
                <div class="col-sm-4">
                <asp:TextBox CssClass="form-control" ID="txtMunicipio" name="lblMunicipio" runat="server" ></asp:TextBox>
                </div>

    			<asp:Label CssClass="col-sm-2 control-label" ID="lblZona" runat="server" Text="Zona:" ></asp:Label>
                <div class="col-sm-4">
                <asp:TextBox CssClass="form-control" ID="txtZona" name="lblZona" runat="server" ></asp:TextBox>
                </div>
                </div>
            </fieldset>
    		
            <fieldset>
    			<legend class="text-center">Contato </legend>
    								
                <div class="form-group">
    			<asp:Label CssClass="col-sm-2 control-label" ID="lblTelefone" runat="server" Text="Telefone:" ></asp:Label>
                <div class="col-sm-4">
                <asp:TextBox CssClass="form-control" ID="txtTelefone" name="lblTelefone" runat="server" ></asp:TextBox>
                </div>
                </div>

                <div class="form-group">
    			<asp:Label CssClass="col-sm-2 control-label" ID="lblCelular" runat="server" Text="Celular:" ></asp:Label>
                <div class="col-sm-4">
                <asp:TextBox CssClass="form-control" ID="txtCelular" name="lblCelular" runat="server" ></asp:TextBox>    
                </div>
                </div>			

                <div class="form-group">
    			<asp:Label CssClass="col-sm-2 control-label" ID="lblEmail" runat="server" Text="E-mail:" ></asp:Label>
                <div class="col-sm-4">
                <asp:TextBox CssClass="form-control" ID="txtEmail" name="lblEmail" runat="server" ></asp:TextBox>
                </div>
                </div>
    			
                <div class="form-group">	
    			<asp:Label CssClass="col-sm-2 control-label" ID="lblOutros" runat="server" Text="Outros:" ></asp:Label>
                <div class="col-sm-4">
                <asp:TextBox CssClass="form-control" ID="txtOutros" name="lblOutros" runat="server" ></asp:TextBox>
                </div>
                </div>                                                              
            </fieldset>                 

            <fieldset>
    			<legend class="text-center"> Programa Social </legend>
                <div class="form-group">
                <asp:Label ID="lblProgramaSocial"  runat="server" Text="Participa de um programa social?" ></asp:Label>
                <asp:RadioButtonList ID="rbProgamaSocial" runat="server">
                    <asp:ListItem Value="Sim" Text="Sim" />
                    <asp:ListItem Value="não" Text="Não" />
                </asp:RadioButtonList>
                </div>
                
                

    			<!--"caso o usuario aperte em "sim" devera abrir os seguintes campos: (java script/ jquery" -->	
                <div class="form-group">
                <asp:Label ID="nomeProgramaSocial" runat="server" Text="Nome" CssClass="col-sm-2 control-label"></asp:Label>
                    <div class="col-sm-2">
                <asp:DropDownList ID="ddlProgramaSocial" name="nomeProgramaSocial" runat="server" CssClass="form-control">          
                </asp:DropDownList>
                        </div>
                </div>
            </fieldset>
    				<!--<span id="msg"> Mensagem </span> -->
    	    <div class="col-sm-6 pull-right">
            <asp:Button ID="btnVoltar" runat="server" Text="Voltar" CssClass="btn btn-lg btn-primary"/>
            <asp:Button ID="btnLimpar" Text="Limpar" runat="server" OnClick="btnLimpar_Click" CssClass="btn btn-lg btn-primary"/>
            <asp:Button ID="btnCadastrar" Text="Cadastrar" runat="server" OnClick="btnCadastrar_Click" CssClass="btn btn-lg btn-primary"/>				                    
                </div>
    				
             <!-- Fim do Código -->
        </form>
    </div>
    </div>
    </div>

</asp:Content>
