﻿<%@ Page Title="" Language="C#" MasterPageFile="~/View/layout.Master" AutoEventWireup="true" CodeBehind="alterar_disciplina.aspx.cs" Inherits="geducSite.View.altear_disciplina" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Conteudo" runat="server">
                
    <div class="row">
        <div class="col-sm-8">
        <div class="box-content">
            <form id="form1" runat="server" class="form-horizontal">
                
                <fieldset>
                    <legend class="text-center">Alterar Disciplina</legend>
                    
                    <div class="form-group">
                        <asp:Label CssClass="col-sm-2 control-label" ID="lblCodigoDisciplina" runat="server" Text="Codigo Disciplina:"></asp:Label>
                        <div class="col-sm-4">
                            <asp:TextBox ID="txtCodigoDisciplina" runat="server" CssClass="form-control"></asp:TextBox>
                            <asp:Button ID="btnBuscar" runat="server" CssClass="btn btn-xs btn-primary" OnClick="btnBuscar_Click" Text="Preencher automaticamente" />
                       </div>

                       <asp:Label CssClass="col-sm-2 control-label" ID="lblNomeDisciplina" runat="server" Text="Nome Disciplina:"></asp:Label>
                        <div class="col-sm-4">
                            <asp:TextBox ID="txtNomeDisciplina" runat="server"></asp:TextBox>
                        </div>

                    </div>
                    
                    <div class="form-group">
                        <asp:Label CssClass="col-sm-2 control-label" ID="lblCargaHorariaDisciplina" runat="server" Text="Carga Horaria Disciplina:"></asp:Label>

                        <div class="col-sm-2">
                            <asp:TextBox ID="txtCargaHorariaDisciplina" runat="server"></asp:TextBox>
                        </div>
                    </div>
                </fieldset>
                
                <p><span id="msg" runat="server">&nbsp;</span></p>
                
                <asp:Button ID="btnCadastrar" CssClass="btn btn-primary btn-lg" runat="server" Text="Alterar" OnClick="btnAlterar_Click" />
            </form>
        </div>
        </div>
    </div>

</asp:Content>
