﻿<%@ Page Title="" Language="C#" MasterPageFile="~/View/layout.Master" AutoEventWireup="true" CodeBehind="consulta_nota.aspx.cs" Inherits="geducSite.View.consulta_nota" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Conteudo" runat="server">
<div class="row">
            <div class="col-sm-8">
                <div class="box-content">
    <h1 class="page-header text-center">Consultar Nota</h1>

    <form>
        <asp:TextBox runat="server" ID="txtBuscar" />
        <asp:DropDownList runat="server" ID="ddlTipoDeBusca">
            <asp:ListItem Value="1">matricula do aluno</asp:ListItem>
            <asp:ListItem Value="2">nota</asp:ListItem>
            <asp:ListItem Value="3">disciplina</asp:ListItem>
            <asp:ListItem Value="4">data</asp:ListItem>
        </asp:DropDownList>
        <asp:Button ID="btnBuscar" Text="listar" runat="server" OnClick="btnBuscar_Click" />
    </form>

    <div id="DivBusca" runat="server">
        <% foreach(var item in BuscarNotas()){%>
        <div>
            <label>Aluno nome</label>
            <label><%: item.aluno.nome %></label>
            
            <label>Aluno matricula</label>
            <label><%: item.aluno.matricula %></label>
            
            <label>Disciplina nome</label>
            <label><%: item.disciplina.nome %></label>
            
            <label>Disciplina codigo</label>
            <label><%: item.disciplina.codigo %></label>
            
            <label>Nota</label>
            <label><%: item.nota %></label>
            
            <label>Origem</label>
            <label><%: item.origem %></label>
            
            <label>Data</label>
            <label><%: item.data %></label>
        </div>
        <% } %>

    </div>
    </div>
    </div>
    </div>
</asp:Content>
