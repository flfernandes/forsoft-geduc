﻿<%@ Page Title="" Language="C#" MasterPageFile="~/View/layout.Master" AutoEventWireup="true" CodeBehind="cadastro_cargo.aspx.cs" Inherits="geducSite.View.cadastro_cargo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Conteudo" runat="server">
    <fieldset>
        <legend>Cargo</legend>

        <form runat="server">
            <asp:Label ID="Label1" runat="server" Text="Cargo"></asp:Label>
            <asp:TextBox runat="server" ID="txtCargo"></asp:TextBox>
            <br />
            <br />
            <asp:Label ID="Label2" runat="server" Text="Funcao"></asp:Label>
            <asp:TextBox runat="server" ID="txtFuncao"></asp:TextBox>
            <br />
            <br />
            <asp:Label ID="msg" runat="server" Text=" "></asp:Label>  <br />
            <br />  <br />
            <br />
            <asp:Button runat="server" ID="btnCadastrar" Text="Cadastrar" OnClick="btnCadastrar_Click" />
        </form>
    </fieldset>

</asp:Content>
