﻿using geducSite.Models;
using geducSite.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace geducSite.View
{
    public partial class consultar_responsavel1 : System.Web.UI.Page
    {

        protected IEnumerable<Responsavel> todosOsResp;
        protected IEnumerable<Responsavel> retorno;

        protected void Page_Load(object sender, EventArgs e)
        {
            todosOsResp = new ResponsavelDAO().listarResponsavel();
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            BuscarResponsavel();
        }

        protected IEnumerable<Responsavel> BuscarResponsavel()
        {
            int valor = Convert.ToInt32(ddlTipoDeBusca.SelectedValue);
            switch (valor)
            {
                case 1:
                    retorno = todosOsResp.Where(x => x.documento.cpf == txtBuscar.Text);
                    DivBusca.Visible = true;
                    break;

                case 2:
                    retorno = todosOsResp.Where(x => x.matriculaA == txtBuscar.Text);
                    DivBusca.Visible = true;
                    break;

                case 3:
                    retorno = todosOsResp.Where(x => x.nome == txtBuscar.Text);
                    DivBusca.Visible = true;
                    break;

                case 4:
                    retorno = todosOsResp.Where(x => x.sexo == txtBuscar.Text);
                    DivBusca.Visible = true;
                    break;

                case 5:
                    retorno = todosOsResp.Where(x => x.documento.rg == txtBuscar.Text);
                    DivBusca.Visible = true;
                    break;
            }

            return retorno;
        }
    }
}