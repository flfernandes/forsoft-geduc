﻿using geducSite.Models;
using geducSite.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace geducSite.View
{
    public partial class listar_funcionario : System.Web.UI.Page
    {
        protected IEnumerable<Funcionario> todosOsFuncionario;
        protected void Page_Load(object sender, EventArgs e)
        {
            geducSite.Models.Login login = (geducSite.Models.Login)Session["login"];

            if (login == null)
            {
                if (Request.Cookies["login"] != null)
                {
                    HttpCookie coockie = Request.Cookies["login"];
                    string ck = coockie.Value.ToString();

                    Session["login"] = new UtilDAO().SessionLogin(ck);
                }
                else
                {
                    Response.Redirect("http://www.projetogeduc.com.br:8080/geduc/login.jsp");
                }
            }
            else if (Util.ValidarAcesso(login.perfilAcesso, "listar_funcionario.aspx") == false)
            {
                Response.Redirect("http://www.projetojeduc.com.br:8080/geduc/nao_pode_acessar.jsp");
            }
            //
            //
            //
            //fim session loginss
            //
            //
            //
            //

            if (Session["ListaFuncionarios"] != null)
            {
                todosOsFuncionario = (IEnumerable<Funcionario>)Session["ListaFuncionarios"];
            }
            else
            {
                todosOsFuncionario = new FuncionarioDAO().Listar();
                Session["ListaFuncionarios"] = todosOsFuncionario;
                Session.Timeout = 6000;
            }
        }

        protected IEnumerable<Funcionario> ListarFuncionaio()
        {
            return todosOsFuncionario.OrderByDescending(x => x.nome);
        }
        
    }
}