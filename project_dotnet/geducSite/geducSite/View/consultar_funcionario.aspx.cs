﻿using geducSite.Models;
using geducSite.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace geducSite.View
{
    public partial class consultar_funcionario : System.Web.UI.Page
    {
        protected IEnumerable<Funcionario> todosOsFuncionario;
        protected IEnumerable<Funcionario> retorno;

        protected void Page_Load(object sender, EventArgs e)
        {
            geducSite.Models.Login login = (geducSite.Models.Login)Session["login"];

            if (login == null)
            {
                if (Request.Cookies["login"] != null)
                {
                    HttpCookie coockie = Request.Cookies["login"];
                    string ck = coockie.Value.ToString();

                    Session["login"] = new UtilDAO().SessionLogin(ck);
                }
                else
                {
                    Response.Redirect("http://www.projetogeduc.com.br:8080/geduc/login.jsp");
                }
            }
            else if (Util.ValidarAcesso(login.perfilAcesso, "consultar_funcionario.aspx") == false)
            {
                Response.Redirect("http://www.projetojeduc.com.br:8080/geduc/nao_pode_acessar.jsp");
            }
            //
            //
            //
            //fim session loginss
            //
            //
            //
            //


            if (Session["ListaFuncionarios"] != null)
            {
                todosOsFuncionario = (IEnumerable<Funcionario>)Session["ListaFuncionarios"];
            }
            else
            {
                todosOsFuncionario = new FuncionarioDAO().Listar();
                Session["ListaFuncionarios"] = todosOsFuncionario;
                Session.Timeout = 6000;
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            BuscarFuncionarios();
        }

        protected IEnumerable<Funcionario> BuscarFuncionarios()
        {
            int valor = Convert.ToInt32(ddlTipoDeBusca.SelectedValue);
            if (txtBuscar.Text.Length != 0)
            {
                switch (valor)
                {
                    case 1:
                        retorno = todosOsFuncionario.Where(x => x.documento.cpf.ToUpper().Contains(txtBuscar.Text.ToUpper()));
                        if (retorno.Count() > 0)
                            DivBusca.Visible = true;
                        break;
                    case 2:
                        retorno = todosOsFuncionario.Where(x => x.matricula.ToUpper().Contains(txtBuscar.Text.ToUpper()));
                        if (retorno.Count() > 0)
                            DivBusca.Visible = true;
                        break;
                    case 3:
                        retorno = todosOsFuncionario.Where(x => x.nome.ToUpper().Contains(txtBuscar.Text.ToUpper()));
                        if (retorno.Count() > 0)
                            DivBusca.Visible = true;
                        break;
                    case 4:
                        retorno = todosOsFuncionario.Where(x => x.sexo.ToUpper().Contains(txtBuscar.Text.ToUpper()));
                        DivBusca.Visible = true;
                        break;
                    case 5:
                        retorno = todosOsFuncionario.Where(x => Convert.ToString(x.idFuncionario).ToUpper().Contains(txtBuscar.Text.ToUpper()));
                        if(retorno.Count() > 0)
                            DivBusca.Visible = true;
                        break;
                    case 6:
                        retorno = todosOsFuncionario.Where(x => x.documento.rg.ToUpper().Contains(txtBuscar.Text.ToUpper()));
                        if (retorno.Count() > 0)
                            DivBusca.Visible = true;
                        break;
                    case 7:
                        retorno = todosOsFuncionario.Where(x => x.situacao.ToUpper().Contains(txtBuscar.Text.ToUpper()));
                        if (retorno.Count() > 0)
                            DivBusca.Visible = true;
                        break;
                }
            }
            return retorno;
        }
    }


}