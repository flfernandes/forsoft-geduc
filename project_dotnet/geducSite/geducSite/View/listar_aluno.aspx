﻿<%@ Page Title="" Language="C#" MasterPageFile="~/View/layout.Master" AutoEventWireup="true" CodeBehind="listar_aluno.aspx.cs" Inherits="geducSite.View.listar_aluno" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Conteudo" runat="server">
    <div class="row">
            <div class="col-sm-8">
                <div class="box-content no-padding">

                    <h1 class="page-header text-center">Listar Alunos</h1>
                
                    <table class="table table-bordered table-striped table-hover table-heading table-datatable" id="datatable-1">
            <tr>
                <th>Nome</th>
                <th>Sexo</th>
                <th>Situacao</th>    
                <th>Telefone</th>
                <th>CPF</th>    
                <th>RG</th> 
                
            </tr>
            <%foreach(var lista in listaA()){ %>
            <tr>
                <td><%: lista.nome %></td>
                <td><%: lista.sexo %></td>
                <td><%: lista.situacao %></td>
                <td><%: lista.contato.telefoneFixo %></td>
                <td><%: lista.documento.cpf %></td>
                <td><%: lista.documento.rg %></td>
            </tr>
            <%} %>
    </table>
    </div></div></div>

<script src="../vendor/devoops/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    // Run Datables plugin and create 3 variants of settings
    function AllTables() {
        TestTable1();
        TestTable2();
        TestTable3();
        LoadSelect2Script(MakeSelect2);
    }
    function MakeSelect2() {
        $('select').select2();
        $('.dataTables_filter').each(function () {
            $(this).find('label input[type=text]').attr('placeholder', 'Search');
        });
    }
    $(document).ready(function () {
        // Load Datatables and run plugin on tables 
        LoadDataTablesScripts(AllTables);
        // Add Drag-n-Drop feature
        WinMove();
    });
</script>

</asp:Content>
