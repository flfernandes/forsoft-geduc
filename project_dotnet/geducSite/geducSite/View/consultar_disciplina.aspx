﻿<%@ Page Title="" Language="C#" MasterPageFile="~/View/layout.Master" AutoEventWireup="true" CodeBehind="consultar_disciplina.aspx.cs" Inherits="geducSite.View.consultar_disciplina" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Conteudo" runat="server">
    <div class="row">
            <div class="col-sm-8">
                <div class="box-content">

    <h1>Funcion&aacute;rio </h1>
    
    <form id="Formulario" runat="server">
    
        <asp:Label Text="Buscar" runat="server" />
    
            <asp:TextBox ID="txtBuscar" runat="server" />
            <asp:DropDownList ID="ddlTipoDeBusca" CssClass="color0" runat="server">
                <asp:ListItem Value="1">Nome</asp:ListItem>
                <asp:ListItem Value="2">Codigo</asp:ListItem>
                <asp:ListItem Value="3">Carga Horaria</asp:ListItem>
            </asp:DropDownList>
            <asp:Button ID="btnBuscar" runat="server" CssClass="btn btn-primary" Text="Buscar" OnClick="btnBuscar_Click" />
    </form>




    <div id="DivBusca" runat="server" visible="false">

        <% foreach (var lista in BuscarDisciplina())
           { %>
        
            <asp:Label runat="server" Text="Nome:"></asp:Label>
            <label><%: lista.nome %></label>
            
            <asp:Label runat="server" Text="Codigo:"></asp:Label>
            <label><%: lista.codigo %></label>

            <asp:Label runat="server" Text="Carga Horaria:"></asp:Label>
            <label><%: lista.cargaHoraria %></label>
            
        <% } %>
    </div>

    </div>
    </div>
    </div>
</asp:Content>
