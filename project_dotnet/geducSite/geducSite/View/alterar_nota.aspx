﻿<%@ Page Title="" Language="C#" MasterPageFile="~/View/layout.Master" AutoEventWireup="true" CodeBehind="alterar_nota.aspx.cs" Inherits="geducSite.View.alterar_nota" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Conteudo" runat="server">
    

    <div class="row">
            <div class="col-sm-8">
                <div class="box-content">
                <form id="frmNota" runat="server" class="form-horizontal">
        
        <fieldset>
            <legend class="text-center">Alterar Notas</legend>       
            
            <div class="form-group">
                <asp:Label ID="Label1" runat="server" Text="identificação" CssClass="col-sm-2 control-label" />
                <div class="col-sm-4">
                <asp:TextBox ID="txtid" runat="server" CssClass="form-control"/>
                </div>
                <div class="col-sm-2 pull-right">
                <asp:Button ID="btnBuscar" Text="Preencher" runat="server" OnClick="btnBuscar_Click"  CssClass="btn btn-primary btn-lg"/>
                </div>
            </div>

            <div class="form-group">
            
                <asp:Label ID="lblDate" runat="server" Text="Data" CssClass="col-sm-2 control-label" />
                <div class="col-sm-4">
                <asp:TextBox ID="txtDate" runat="server" CssClass="form-control" />
                </div>

                <asp:Label ID="lblNota" runat="server" Text="Nota " CssClass="col-sm-2 control-label" />
                <div class="col-sm-4">
                <asp:TextBox ID="txtNota" runat="server" CssClass="form-control" />
                </div>

            </div>
            
            <div class="form-group">
                <asp:Label ID="lblPeriodo" runat="server" Text="Periodo" CssClass="col-sm-2 control-label" />
                <div class="col-sm-4">
                <asp:TextBox ID="txtPeriodo" runat="server" CssClass="form-control" />
                </div>
                
                <asp:Label ID="lblOrigem" runat="server" Text="Origem" CssClass="col-sm-2 control-label" />
                <div class="col-sm-4">
                <asp:TextBox ID="txtOrigem" runat="server" CssClass="form-control" />
                </div>
            </div>
        </fieldset>
        
        <asp:Label ID="lblMensagem" runat="server" />
        <div class="col-sm-4 pull-right">
        <asp:Button ID="btnAlterarNota"  runat="server" Text="Alterar Nota" OnClick="btnAlterarNota_Click" CssClass="btn btn-primary btn-lg" />
            </div>
    </form>
</div>
</div>
</div>
</asp:Content>
