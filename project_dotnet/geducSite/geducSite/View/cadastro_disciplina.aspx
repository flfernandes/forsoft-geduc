﻿<%@ Page Title="" Language="C#" MasterPageFile="~/View/layout.Master" AutoEventWireup="true" CodeBehind="cadastro_disciplina.aspx.cs" Inherits="geducSite.View.cadastro_disciplina" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Conteudo" runat="server">
    <div class="row">
            <div class="col-sm-8">
        <div class="box-content">
            <form id="form1" runat="server" class="form-horizontal">
                
                <fieldset>
                    <legend class="text-center">Disciplina</legend>
                    <div class="form-group">                        
                        <asp:Label CssClass="col-sm-2 control-label" ID="lblNomeDisciplina" runat="server" Text="Disciplina"></asp:Label>
                        <div class="col-sm-4">
                            <asp:TextBox ID="txtNomeDisciplina" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <asp:Label CssClass="col-sm-2 control-label" ID="lblCodigoDisciplina" runat="server" Text="Codigo"></asp:Label>
                        <div class="col-sm-4">
                            <asp:TextBox ID="txtCodigoDisciplina" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>

                        <asp:Label CssClass="col-sm-3 control-label" ID="lblCargaHorariaDisciplina" runat="server" Text="Carga Horaria"></asp:Label>
                        <div class="col-sm-3">
                            <asp:TextBox ID="txtCargaHorariaDisciplina" runat="server" TextMode="Number" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    
                    
                    <div class="form-group">
                        
                    </div>
                </fieldset>
                
                <p><span id="msg" runat="server"></span></p>
                
                <asp:Button ID="btnCadastrar" runat="server" Text="Cadastrar" OnClick="btnCadastrar_Click" CssClass="btn btn-lg btn-primary" />
            </form>
        </div>
        </div>
    </div>
</asp:Content>
