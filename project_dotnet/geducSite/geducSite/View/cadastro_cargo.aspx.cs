﻿using geducSite.Models;
using geducSite.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace geducSite.View
{
    public partial class cadastro_cargo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            /*
            HttpCookie cookie = new HttpCookie("login");
            cookie.Value = "1";
            DateTime dtNow = DateTime.Now;
            TimeSpan tsMinute = new TimeSpan(0, 0, 1, 0);
            cookie.Expires = dtNow + tsMinute;
            //Adiciona o cookie
            Response.Cookies.Add(cookie);
            */
            //
            //
            //

            geducSite.Models.Login login = (geducSite.Models.Login)Session["login"];

            if (login == null)
            {
                if (Request.Cookies["login"] != null)
                {
                    HttpCookie coockie = Request.Cookies["login"];
                    string ck = coockie.Value.ToString();

                    Session["login"] = new UtilDAO().SessionLogin(ck);
                }
                else
                {
                    Response.Redirect("http://www.projetogeduc.com.br:8080/geduc/login.jsp");
                }
            }
            else if (Util.ValidarAcesso(login.perfilAcesso, "cadastro_aluno.aspx") == false)
            {
                Response.Redirect("http://www.projetojeduc.com.br:8080/geduc/nao_pode_acessar.jsp");
            }
            //
            //
            //
            //fim session loginss
            //
            //
            //
            //


        }

        protected void btnCadastrar_Click(object sender, EventArgs e)
        {
            msg.Text = String.Empty;

            String[] campos = new String[] { txtCargo.Text, txtFuncao.Text };

            if (!Validador.seAlgumVazio(campos))
            {
                if (Validador.tamanhoMax(campos, 50))
                {
                    try
                    {
                        Cargo c = new Cargo();
                        c.cargo = txtCargo.Text;
                        c.funcao = txtFuncao.Text;
                        new CargoDAO().Cadastrar(c);

                        txtCargo.Text = String.Empty;
                        txtFuncao.Text = String.Empty;
                        msg.Text = "Cadastrado com sucesso!!!";
                    }
                    catch (Exception erro)
                    {
                        msg.Text = "erro:" + erro + ".";
                    }
                }
                else msg.Text = "Ultrapassou o limite máximo de caracteres";
            }
            else msg.Text = "Campo não preenchido";
        }
    }
}