﻿<%@ Page Title="" Language="C#" MasterPageFile="~/View/layout.Master" AutoEventWireup="true" CodeBehind="cadastro_aluno.aspx.cs" Inherits="geducSite.View.cadastro_aluno" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Conteudo" runat="server">
    <div class="row">
            <div class="col-sm-8">
                
    <div class="box-content">
        <form id="form1" runat="server" class="form-horizontal">
        
            <!-- Início do Código -->
            <h1 class="page-header text-center">Cadastro de Aluno<asp:Label ID="msg" runat="server"></asp:Label></h1>
            <fieldset>
                <legend class="text-center">Login</legend>
                    
                    <div class="form-group">
                    <asp:Label ID="lblNomeUsuario" runat="server" Text="Usu&aacute;rio:" CssClass="col-sm-2 control-label"></asp:Label>
                    <div class="col-sm-4">
                    <asp:TextBox ID="txtUsuario" name="lblNomeUsuario" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
        								
        		    <asp:Label ID="lblSenha" runat="server" Text="Senha:" CssClass="col-sm-2"></asp:Label>
                    <div class="col-sm-4">
                    <asp:TextBox ID="txtSenha" name="lblSenha" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    </div>
            </fieldset>			
    		
            <fieldset>
    			<legend class="text-center">Dados Pessoais</legend>
                <div class="form-group">
                <asp:Label ID="lblNomeAluno" runat="server" Text="Nome:" CssClass="col-sm-2 control-label"></asp:Label>
                <div class="col-sm-8">
                <asp:TextBox ID="txtNomeAluno" name="lblNomeAluno" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                </div>
         

                <div class="row">
                <div class="form-group">
                    <asp:Label ID="lblDataNascimento" runat="server" Text="Data de Nascimento:" CssClass="col-sm-2 control-label"></asp:Label>
                    <div class="col-sm-4">
                        <asp:TextBox ID="txtDataNascimento" name="lblDataNascimento" runat="server" CssClass="form-control" TextMode="date"></asp:TextBox>
                    </div>
        							
        			<asp:Label ID="lblSexo" runat="server" Text="Sexo:" CssClass="col-sm-1 control-label"></asp:Label>	
                    <div class="col-sm-4">
                        <asp:RadioButtonList ID="rbSexo" runat="server">
                            <asp:ListItem Value="Masculino" Text="Masculino" CssClass="checkbox-inline" />
                            <asp:ListItem Value="Feminino" Text="Feminino" CssClass="checkbox-inline" /> 
                        </asp:RadioButtonList>
                    </div>
                </div>
                    </div>

                <div class="form-group">

    			<asp:Label ID="lblNaturalidade" runat="server" Text="Naturalidade:" CssClass="col-sm-2 control-label"></asp:Label>
                <div class="col-sm-4">
                <asp:TextBox ID="txtNaturalidade" name="lblNaturalidade" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                

                <asp:Label ID="lblNacionalidade" runat="server" Text="Nacionalidade:" CssClass="col-sm-2 control-label"></asp:Label>
                
                <div class="col-sm-4">
                    <asp:TextBox ID="txtNacionalidade" name="lblNacionalidade" runat="server" CssClass="form-control" ></asp:TextBox>
                </div>

                </div>
    			
                
                <div class="form-group">
                
                <asp:Label ID="lblNomePai" runat="server" Text="Pai:" CssClass="col-sm-2 control-label"></asp:Label>
                <div class="col-sm-4">
                <asp:TextBox ID="txtNomePai" name="lblNomePai" runat="server" CssClass="form-control" ></asp:TextBox>
                </div>

                <asp:Label ID="lblNomeMae" runat="server" Text="M&atilde;e:" CssClass="col-sm-2 control-label"></asp:Label>
                <div class="col-sm-4">
                    <asp:TextBox ID="txtNomeMae" name="lblNomeMae" runat="server" CssClass="form-control"></asp:TextBox> 
                </div>

                </div>

                <div class="form-group">
    															
    			<asp:Label ID="lblEtnia" runat="server" Text="Etnia:" CssClass="col-sm-2 control-label"></asp:Label>
                <div class="col-sm-4">
                <asp:DropDownList ID="ddlEtnia" name="lblEtnia" runat="server" CssClass="form-control">
                    <asp:ListItem>Branco(a)</asp:ListItem>
                    <asp:ListItem>Negro(a)</asp:ListItem>
                    <asp:ListItem>Amarelo(a)</asp:ListItem>
                    <asp:ListItem>Pardo</asp:ListItem>
                    <asp:ListItem>Ind&iacute;gena</asp:ListItem>
                    <asp:ListItem>Sem Declara&ccedil;&atilde;o</asp:ListItem>
                </asp:DropDownList>
                </div>
                

                <asp:Label ID="lblEstadoCivil" runat="server" Text="Estado Civil:" CssClass="col-sm-2 control-label"></asp:Label>
                <div class="col-sm-4">
                <asp:DropDownList ID="ddlEstadoCivil" name="lblEstadoCivil" runat="server" CssClass="form-control">
                    <asp:ListItem>Solteiro (a)</asp:ListItem>
                    <asp:ListItem>Casado (a)</asp:ListItem>
                    <asp:ListItem>Divorciado (a)</asp:ListItem>
                    <asp:ListItem>Vi&uacute;vo (a)</asp:ListItem>
                    <asp:ListItem>Sem Declara&ccedil;&atilde;o</asp:ListItem>
                </asp:DropDownList>
                </div>

                </div>
                
                <div class="row">
                <div class="form-group">
                <asp:Label ID="lblEscolaridade" runat="server" Text="N&iacute;vel de Escolaridade:" CssClass="col-sm-2"></asp:Label>
                <div class="col-sm-4">
                <asp:DropDownList ID="ddlEscolaridade" name="lblEscolaridade" runat="server" CssClass="form-control">
                    <asp:ListItem>Escolha uma op&ccedil;&atilde;o</asp:ListItem>
                    <asp:ListItem>Superior</asp:ListItem>
                    <asp:ListItem>Superior Incompleto</asp:ListItem>
                    <asp:ListItem>T&eacute;cnico</asp:ListItem>
                    <asp:ListItem>T&eacute;cnico Incompleto</asp:ListItem>
                    <asp:ListItem>M&eacute;dio</asp:ListItem>
                    <asp:ListItem>M&eacute;dio Incompleto</asp:ListItem>
                    <asp:ListItem>Fundamental</asp:ListItem>
                    <asp:ListItem>Fundamental Incompleto</asp:ListItem>
                </asp:DropDownList>
                </div>
                    </div>
                </div>

                <div class="row">
                <div class="form-group">
                <asp:Label ID="lblNecessidadeEspecial"  runat="server" Text="Possui alguma necessidade especial?" CssClass="col-sm-4 control-label"></asp:Label>
                <div class="col-sm-8">
                <asp:RadioButtonList ID="rbNecessidadeEspecial"	runat="server">
                    <asp:ListItem Value="Sim" Text="Sim" CssClass="checkbox-inline" />
                    <asp:ListItem Value="Não" Text="Não" CssClass="checkbox-inline" />
                </asp:RadioButtonList>
                </div>
                    </div>
                </div>
            </fieldset>    								
    		
            <fieldset>
                <legend class="text-center">Aluno</legend>

                <div class="form-group">
                    <asp:Label ID="lblMatricula" runat="server" Text="Matr&iacute;cula" CssClass="col-sm-2 control-label"></asp:Label>
                    <div class="col-sm-4">
                    <asp:TextBox ID="txtMatricula" name="lblMatricula" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
        								
                    <asp:Label ID="lblSituacao" runat="server" Text="Situa&ccedil;&atilde;o:" CssClass="col-sm-2 control-label"></asp:Label>
                    <div class="col-sm-4">
                    <asp:DropDownList ID="ddlSituacao" name="lblSituacao" runat="server" CssClass="form-control">
                        <asp:ListItem>Escolha uma op&ccedil;&atilde;o</asp:ListItem>
                        <asp:ListItem Selected="True" >Ativo</asp:ListItem>
                        <asp:ListItem>Inativo</asp:ListItem>                      
                    </asp:DropDownList> 
                    </div>
                </div>
            </fieldset>    								
    					
            <fieldset>
    			<legend class="text-center">Documenta&ccedil;ao</legend>
                <div class="row">
                <div class="form-group">
    			<asp:Label ID="lblCPF" runat="server" Text="CPF:" CssClass="col-sm-2 control-label"></asp:Label>
                <div class="col-sm-2">
                <asp:TextBox ID="txtCPF" name="lblCPF" runat="server" CssClass="form-control"></asp:TextBox></div>
                </div>
                    </div>
                
                <div class="row">
                <div class="form-group">
                <asp:Label ID="lblRG" runat="server" Text="RG:" CssClass="col-sm-2 control-label"></asp:Label>
                <div class="col-sm-2">
                <asp:TextBox ID="txtRG" name="lblRG" runat="server" CssClass="form-control"></asp:TextBox>
                </div>    			

    			<asp:Label ID="lblExpedido" runat="server" Text="Data de Expedi&ccedil;&atilde;o:" CssClass="col-sm-3 control-label"></asp:Label>

                <div class="col-sm-3">
                <asp:TextBox ID="txtDataExpedicao"  name="lblExpedido" runat="server"  TextMode="date" CssClass="form-control"></asp:TextBox>   
                </div>
                </div> 
                    </div>
                
                <div class="row">
                
                <div class="form-group">
    			<asp:Label ID="lblOrgao" runat="server" Text="Org&atilde;o Expedidor:" CssClass="col-sm-2 control-label"></asp:Label>
                <div class="col-sm-2">
                <asp:TextBox ID="txtOrgao" name="lblOrgao" runat="server" CssClass="form-control"></asp:TextBox>	
                </div>
                </div>
                    </div>			
            </fieldset>

            <fieldset>
    			<legend class="text-center">Certid&atilde;o de Nascimento:</legend>
    			
                <div class="form-group">
                <asp:Label ID="lblNumCertidaoNascimento" runat="server" Text="N&uacute;mero:" CssClass="col-sm-2 control-label"></asp:Label>
                <div class="col-sm-2">
                <asp:TextBox ID="txtNumCertidaoNascimento" name="lblNumCertidaoNascimento" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
    			

    			<asp:Label ID="lblLivroCertidaoNascimento" runat="server" Text="Livro:" CssClass="col-sm-2 control-label"></asp:Label>
                <div class="col-sm-3">
                <asp:TextBox ID="txtLivroCertidaoNascimento" name="lblLivroCertidaoNascimento" runat="server" CssClass="form-control"></asp:TextBox>
                </div>

                </div>

                <div class="form-group">

    			<asp:Label ID="lblFolhaCertidaoNascimento" runat="server" Text="Folha:" CssClass="col-sm-2 control-label"></asp:Label>
                <div class="col-sm-2">
                <asp:TextBox ID="txtFolhaCertidaoNascimento" name="lblFolhaCertidaoNascimento" runat="server" CssClass="form-control"></asp:TextBox>    			
                </div>

    			<asp:Label ID="lblDataCertidaoNascimento" runat="server" Text="Data de Emiss&atilde;o:" CssClass="col-sm-2 control-label"></asp:Label>
                <div class="col-sm-3">
                <asp:TextBox ID="txtDataCertidaoNascimento" name="lblDataCertidaoNascimento" runat="server" CssClass="form-control" TextMode="date"></asp:TextBox>    			
                </div>
                </div>

                <div class="form-group">
            								
                <asp:Label ID="lblTituloEleitor" runat="server" Text="T&iacute;tulo de Eleitor:" CssClass="col-sm-2 control-label"></asp:Label>
                <div class="col-sm-2">
                <asp:TextBox ID="txtTituloEleitor" name="lblTituloEleitor" runat="server" CssClass="form-control"></asp:TextBox>    		
                </div>
                </div>

                <div class="form-group">

                <asp:Label ID="lblCertificadoReservista" runat="server" Text="Certificado de Reservista:" CssClass="col-sm-2 control-label"></asp:Label>
                
                <div class="col-sm-2">
                <asp:TextBox ID="txtCertificadoReservista" name="lblCertificadoReservista" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                </div>
            </fieldset>
            
            <fieldset>
    			<legend class="text-center">Endere&ccedil;o</legend>
                
                <div class="form-group">
    			<asp:Label ID="lblLogradouro" runat="server" Text="Logradouro" CssClass="col-sm-2 control-label"></asp:Label>
                <div class="col-sm-10">
                <asp:TextBox ID="txtLogradouro" name="lblLogradouro" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                </div>

                <div class="form-group">
    			
                <asp:Label ID="lblNumero" runat="server" Text="N&uacute;mero" CssClass="col-sm-2"></asp:Label>
                <div class="col-sm-2">
                <asp:TextBox ID="txtNumero" name="lblNumero" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
    								
    			<asp:Label ID="lblComplemento" runat="server" Text="Complemento" CssClass="col-sm-2 control-label"></asp:Label>
                <div class="col-sm-4">
                <asp:TextBox ID="txtComplemento" name="lblComplemento" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                </div>

                <div class="form-group">
    			<asp:Label ID="lblBairro" runat="server" Text="Bairro" CssClass="col-sm-1 control-label"></asp:Label>
                <div class="col-sm-3">
                <asp:TextBox ID="txtBairro" name="lblBairro" runat="server" CssClass="form-control"></asp:TextBox>   		
                </div>	

    			<asp:Label ID="lblCidade" runat="server" Text="Cidade" CssClass="col-sm-1 control-label"></asp:Label>
                <div class="col-sm-3">
                <asp:TextBox ID="txtCidade" name="lblCidade" runat="server" ></asp:TextBox>   	
                </div>

                </div>

                <div class="form-group">

                <asp:Label ID="lblUF" runat="server" Text="UF" CssClass="col-sm-1 control-label"></asp:Label>
                <div class="col-sm-2">
                <asp:DropDownList ID="ddlUF" name="lblUF" runat="server" CssClass="form-control">
                    <asp:ListItem></asp:ListItem>
    				<asp:ListItem>AC</asp:ListItem>
                    <asp:ListItem>AL</asp:ListItem> 
                    <asp:ListItem>AP</asp:ListItem> 
                    <asp:ListItem>AM</asp:ListItem> 
                    <asp:ListItem>BA</asp:ListItem> 
                    <asp:ListItem>CE</asp:ListItem> 
                    <asp:ListItem>DF</asp:ListItem> 
                    <asp:ListItem>ES</asp:ListItem> 
                    <asp:ListItem>GO</asp:ListItem> 
                    <asp:ListItem>MA</asp:ListItem> 
                    <asp:ListItem>MT</asp:ListItem> 
                    <asp:ListItem>MS</asp:ListItem> 
                    <asp:ListItem>MG</asp:ListItem> 
                    <asp:ListItem>PA</asp:ListItem> 
                    <asp:ListItem>PB</asp:ListItem> 
                    <asp:ListItem>PR</asp:ListItem> 
                    <asp:ListItem>PE</asp:ListItem> 
                    <asp:ListItem>PI</asp:ListItem> 
                    <asp:ListItem Selected="True">RJ</asp:ListItem> 
                    <asp:ListItem >RN</asp:ListItem> 
                    <asp:ListItem>RS</asp:ListItem> 
                    <asp:ListItem>RO</asp:ListItem> 
                    <asp:ListItem>RR</asp:ListItem> 
                    <asp:ListItem>SC</asp:ListItem> 
                    <asp:ListItem>SP</asp:ListItem> 
                    <asp:ListItem>SE</asp:ListItem> 
                    <asp:ListItem>TO</asp:ListItem>  	                     
                </asp:DropDownList>
                </div>
    											
    			<asp:Label ID="lblCEP" runat="server" Text="CEP" CssClass="col-sm-1 control-label"></asp:Label>
                <div class="col-sm-2">
                <asp:TextBox ID="txtCEP" name="lblCEP" runat="server" CssClass="form-control"></asp:TextBox>   
                </div>


                <asp:Label ID="lblMunicipio" runat="server" Text="Munic&iacute;pio:" CssClass="col-sm-2 control-label"></asp:Label>
                <div class="col-sm-4">
                <asp:TextBox ID="txtMunicipio" name="lblMunicipio" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                </div>
            
                <div class="form-group">

    			<asp:Label ID="lblZona" runat="server" Text="Zona:" CssClass="col-sm-2 control-label"></asp:Label>
                <div class="col-sm-4">
                <asp:TextBox ID="txtZona" name="lblZona" runat="server" CssClass="form-control"></asp:TextBox>
                </div>

                </div>      
    		</fieldset>
    		
            <fieldset>
    			<legend>Contato</legend>

                <div class="form-group">
    			<asp:Label ID="lblTelefone" runat="server" Text="Telefone:" CssClass="col-sm-2 control-label"></asp:Label>
                <div class="col-sm-4">
                <asp:TextBox ID="txtTelefone" name="lblTelefone" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
    			</div>

                <div class="form-group">
    			<asp:Label ID="lblCelular" runat="server" Text="Celular:" CssClass="col-sm-2 control-label"></asp:Label>
                <div class="col-sm-4">
                <asp:TextBox ID="txtCelular" name="lblCelular" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                </div>

                <div class="form-group">
    			<asp:Label ID="lblEmail" runat="server" Text="E-mail:" CssClass="col-sm-2 control-label"></asp:Label>
                <div class="col-sm-4">
                <asp:TextBox ID="txtEmail" name="lblEmail" runat="server" CssClass="form-control"></asp:TextBox>    	
                </div>
                </div>

    			<div class="form-group">
    			<asp:Label ID="lblOutros" runat="server" Text="Outros:" CssClass="col-sm-2 control-label"></asp:Label>
                <div class="col-sm-4">
                <asp:TextBox ID="txtOutros" name="lblOutros" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                </div>
    		</fieldset>	
            
            <fieldset>
                <legend class="text-center">Curso:</legend>
            
            <div class="form-group">
                <asp:Label ID="lblCurso" runat="server" Text="Selecione o Curso do aluno:" CssClass="col-sm-4 control-label" />
                <div class="col-sm-8">
                <asp:DropDownList ID="ddlCurso" runat="server" class="form-control">
                </asp:DropDownList>
                </div>
                </div>
            </fieldset>

            <fieldset>
    			<legend class="text-center">Programa Social</legend>
                <div class="form-group">
                <asp:Label ID="lblProgramaSocial"  runat="server" Text="Participa de um programa social?" CssClass="col-sm-4 control-label"></asp:Label>
                <div class="col-sm-8">
                <asp:RadioButtonList ID="rbProgamaSocial" runat="server" CssClass="form-control">
                    <asp:ListItem Value="Sim" Text="Sim" />
                    <asp:ListItem Value="não" Text="Não" />
                </asp:RadioButtonList>
                </div>
                </div>                
                
                <div class="form-group">
    			<!--"caso o usuario aperte em "sim" devera abrir os seguintes campos: (java script/ jquery" -->	
                <asp:Label ID="nomeProgramaSocial" runat="server" Text="Nome:" CssClass="col-sm-2 control-label"></asp:Label>
                <div class="col-sm-4">
                <asp:DropDownList ID="ddlProgramaSocial" name="nomeProgramaSocial" runat="server" CssClass="form-control">          
                </asp:DropDownList>
                </div>
                </div>
            </fieldset>
                
    				<asp:Label ID="msgErro" runat="server" />
    				
                <asp:Button ID="btnVoltar" runat="server" Text="Voltar" />
                <asp:Button ID="btnLimpar" Text="Limpar" runat="server" OnClick="btnLimpar_Click"/>
                <asp:Button ID="btnCadastrar" Text="Cadastrar" runat="server" OnClick="btnCadastrar_Click"/>				                    
    				
             <!-- Fim do Código -->
        </form>
    </div>
    </div>
    </div>
</asp:Content>
