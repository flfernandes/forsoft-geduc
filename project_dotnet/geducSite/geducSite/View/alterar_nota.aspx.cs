﻿using geducSite.Models;
using geducSite.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace geducSite.View
{
    public partial class alterar_nota : System.Web.UI.Page
    {

        protected IEnumerable<Nota> todasAsNotas;

        protected void Page_Load(object sender, EventArgs e)
        {
            /*
            geducSite.Models.Login login = (geducSite.Models.Login)Session["login"];

            if (login == null)
            {
                if (Request.Cookies["login"] != null)
                {
                    HttpCookie coockie = Request.Cookies["login"];
                    string ck = coockie.Value.ToString();

                    Session["login"] = new UtilDAO().SessionLogin(ck);
                }
                else
                {
                    Response.Redirect("http://www.projetogeduc.com.br:8080/geduc/login.jsp");
                }
            }
            else if (Util.ValidarAcesso(login.perfilAcesso, "alterar_nota.aspx") == false)
            {
                Response.Redirect("http://www.projetojeduc.com.br:8080/geduc/nao_pode_acessar.jsp");
            }
             */
            //
            //
            //
            //fim session loginss
            //
            //
            //
            //
            if (Session["todasAsNotas"] != null)
            {
                todasAsNotas = (IEnumerable<Nota>)Session["todasAsNotas"];
            }
            else
            {
                todasAsNotas = new NotaDAO().Listar();
                Session["todasAsNotas"] = todasAsNotas;
                Session.Timeout = 6000;
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            Nota nota = todasAsNotas.SingleOrDefault(x => x.idNota == Convert.ToInt32(txtid.Text));
            if (nota != null)
            {
                txtDate.Text = Convert.ToString(nota.data);
                txtNota.Text = Convert.ToString(nota.nota);
                txtOrigem.Text = nota.origem;
                txtPeriodo.Text = nota.periodo;
            }
        }

        protected void btnAlterarNota_Click(object sender, EventArgs e)
        {
            lblMensagem.Text = String.Empty;

            String[] campos = new String[] { txtNota.Text, txtPeriodo.Text, txtDate.Text };

            if (Validar.tamanhoMax(txtPeriodo.Text, 50) && Validar.tamanhoMax(txtOrigem.Text, 200))
            {
                if (Validar.seSomenteNumero(txtNota.Text) && Validar.seData(txtDate.Text))
                {
                    Nota nota = todasAsNotas.SingleOrDefault(x => x.idNota == Convert.ToInt32(txtid.Text));
                    if (nota != null)
                    {
                        nota.data = Convert.ToDateTime(txtDate.Text);
                        nota.nota = Convert.ToInt32(txtNota.Text);
                        nota.origem = Convert.ToString(txtOrigem.Text);
                        nota.periodo = Convert.ToString(txtPeriodo.Text);

                        new NotaDAO().Alterar(nota);

                        lblMensagem.Text = "Nota alterado com sucesso!";
                    }
                    else lblMensagem.Text = "Nota não cadastrada. Para alterar, cadastre a nota primeiro.";
                }
                else lblMensagem.Text = "Preenchimento de campos inválido.";
            }
            else lblMensagem.Text = "Ultrapassou o limite máximo de caracteres";
        }
    }
}